package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelEstadoCivil
import com.kotlinmark43.sockets.model.EstadoCivil
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/estadocivil")
class EstadoCivilController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveEstadoCivil"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: EstadoCivil): PanelEstadoCivil? {
        try {
            val p = PanelEstadoCivil(message.nombre)
            this.template.convertAndSend("/task/panelEstadoCivil", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveEstadoCivil")
    @SendTo("/task/panelEstadoCivil")
    fun greeting1(@RequestBody v: EstadoCivil): PanelEstadoCivil? {
        try {
            val p = PanelEstadoCivil(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(EstadoCivilController::class.java)
    }
}

