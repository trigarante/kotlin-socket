package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelAreas
import com.kotlinmark43.sockets.model.Areas
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/areas")
class AreasController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveArea"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Areas): PanelAreas? {
        try {
            val p = PanelAreas(message.nombre)
            this.template.convertAndSend("/task/panelAreas", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveArea")
    @SendTo("/task/panelAreas")
    fun greeting1(@RequestBody v: Areas): PanelAreas? {
        try {
            val p = PanelAreas(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(AreasController::class.java)
    }
}

