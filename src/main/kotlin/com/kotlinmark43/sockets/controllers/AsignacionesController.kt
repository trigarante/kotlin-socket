package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelAsignaciones
import com.kotlinmark43.sockets.model.Asignaciones
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/asignaciones")
class AsignacionesController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveAsignaciones"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Asignaciones): PanelAsignaciones? {
        try {
            val p = PanelAsignaciones( message.calificacionRollPlay)
            this.template.convertAndSend("/task/panelAsignaciones", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveAsignaciones")
    @SendTo("/task/panelAsignaciones")
    fun greeting1(@RequestBody v: Asignaciones): PanelAsignaciones? {
        try {
            val p = PanelAsignaciones(v.calificacionRollPlay, v.comentarios, v.idCapacitacion, v.idCoach, v.fechaIngreso, v.idSubarea, v.asistencia)
            logger.info(p)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(AsignacionesController::class.java)
    }
}