package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Capacitacion
import com.kotlinmark43.sockets.model.PanelCapacitacion
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/capacitacion")
class CapacitacionController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCapacitacion"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Capacitacion): PanelCapacitacion? {
        try {
            val p = PanelCapacitacion( message.nombre)
            this.template.convertAndSend("/task/panelCapacitacion", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCapacitacion")
    @SendTo("/task/panelCapacitacion")
    fun greeting1(@RequestBody v: Capacitacion): PanelCapacitacion? {
        try {
            val p = PanelCapacitacion(v.nombre)
            logger.info(p)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CapacitacionController::class.java)
    }
}