package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelPresupuesto
import com.kotlinmark43.sockets.model.Presupuesto
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/presupuesto")
class PresupuestoController @Autowired


constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/savePresupuesto/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Presupuesto) : PanelPresupuesto? {
        try {
            val p = PanelPresupuesto(mensaje.presupuesto, mensaje.activo, mensaje.anio)
            this.template.convertAndSend("/task/panelPresupuesto", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/savePresupuesto/1")  @SendTo("/task/panelPresupuesto")
    fun greeting1(@RequestBody v: Presupuesto): PanelPresupuesto? {
        try {
            val p = PanelPresupuesto(v.presupuesto, v.activo, v.anio)
            logger.info(p.presupuesto, p.activo, p.anio)
            return p
        }
        catch (e: Exception) {
            PresupuestoController.logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(PresupuestoController::class.java)
    }
}