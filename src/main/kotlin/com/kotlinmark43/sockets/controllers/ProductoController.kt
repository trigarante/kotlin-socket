package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Producto
import com.kotlinmark43.sockets.model.PanelProducto
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/producto")
class ProductoController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveProducto"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Producto) : PanelProducto? {
        try {
            val p = PanelProducto(mensaje.nombre, mensaje.activo)
            this.template.convertAndSend("/task/panelProducto", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveProducto")  @SendTo("/task/panelProducto")
    fun greeting1(@RequestBody v: Producto): PanelProducto? {
        try {
            val p = PanelProducto(v.nombre, v.activo)
            logger.info(p.activo)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(ProductoController::class.java)
    }
}