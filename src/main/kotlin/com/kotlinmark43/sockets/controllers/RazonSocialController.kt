package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelRazonSocial
import com.kotlinmark43.sockets.model.RazonSocial
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/razonsocial")
class RazonSocialController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveRazonSocial"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: RazonSocial): PanelRazonSocial? {
        try {
            val p = PanelRazonSocial(message.nombre)
            this.template.convertAndSend("/task/panelRazonSocial", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveRazonSocial")
    @SendTo("/task/panelRazonSocial")
    fun greeting1(@RequestBody v: RazonSocial): PanelRazonSocial? {
        try {
            val p = PanelRazonSocial(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(RazonSocialController::class.java)
    }
}

