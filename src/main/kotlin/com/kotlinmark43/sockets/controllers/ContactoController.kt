package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Contacto
import com.kotlinmark43.sockets.model.PanelContacto
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/contactos")
class ContactoController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveContacto/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Contacto) : PanelContacto? {
        try {
            val p = PanelContacto(mensaje.nombre, mensaje.activo)
            this.template.convertAndSend("/task/panelContactos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveContacto/1")  @SendTo("/task/panelContactos")
    fun greeting1(@RequestBody v: Contacto): PanelContacto? {
        try {
            val p = PanelContacto(v.nombre, v.activo)
            logger.info(p.nombre, p.activo)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(ContactoController::class.java)
    }
}