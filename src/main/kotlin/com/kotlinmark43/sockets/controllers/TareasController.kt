package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelTareas
import com.kotlinmark43.sockets.model.Tareas
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.apache.logging.log4j.LogManager
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import java.lang.Exception


@Controller
@CrossOrigin
@RestController
@RequestMapping("/tareas")
class TareasController @Autowired
constructor(private  val template: SimpMessagingTemplate) {

    // To Sub Area
    @RequestMapping(value = ["/addTareaSubArea"], method = arrayOf(RequestMethod.POST))
    fun postingTareaSubArea(@RequestBody message: Tareas): PanelTareas? {
        try {
            val id = message.idSubArea;
            val panelTarea = PanelTareas(message.idEmpleados, message.idSubArea, message.activo);
            this.template.convertAndSend("/task/tareas/SubArea/"+id, message)
            return  panelTarea;
        }
        catch (e: Exception){
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    @MessageMapping("/addTarea")
    @SendTo("/task/tareas")
    fun postingTarea1(@RequestBody message: Tareas): PanelTareas? {
        try {
            val p = PanelTareas(message.idEmpleados, message.idSubArea);
            logger.info(p.idEmpleados)

            return  p;
        }
        catch (e: Exception){
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    companion object {

        private val logger = LogManager.getLogger(VacantesController::class.java)
    }
}