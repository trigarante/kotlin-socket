package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelSolicitudes
import com.kotlinmark43.sockets.model.Solicitudes
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/solicitudes")
class SolicitudesController @Autowired
    constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveSolicitud/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Solicitudes): PanelSolicitudes? {
        try {
            val p = PanelSolicitudes(message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.vacante,
                    message.reclutador, message.sede)
            this.template.convertAndSend("/task/panel/1", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveSolicitud/1")
    @SendTo("/task/panel/1")
    fun greeting1(@RequestBody v: Solicitudes): PanelSolicitudes? {
        try {
            val p = PanelSolicitudes(v.nombre)
            logger.info(p.nombre)
            logger.info(p.nombre)

            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(SolicitudesController::class.java)
    }
}
