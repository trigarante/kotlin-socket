package com.kotlinmark43.sockets.controllers
import com.kotlinmark43.sockets.model.PanelRollPlay
import com.kotlinmark43.sockets.model.RollPlay
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/rollplay")
class RollPlayController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveRollplay"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: RollPlay): PanelRollPlay? {
        try {
            val p = PanelRollPlay( message.calificacionRollPlay)
            this.template.convertAndSend("/task/panelRollplay", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveRollplay")
    @SendTo("/task/panelRollplay")
    fun greeting1(@RequestBody v: RollPlay): PanelRollPlay? {
        try {
            val p = PanelRollPlay(v.calificacionRollPlay, v.comentarios, v.idCapacitacion, v.idCoach, v.fechaIngreso, v.idSubarea, v.asistencia)
            logger.info(p)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(RollPlayController::class.java)
    }
}