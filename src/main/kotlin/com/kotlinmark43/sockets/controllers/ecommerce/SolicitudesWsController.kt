package com.kotlinmark43.sockets.controllers.ecommerce

import com.kotlinmark43.sockets.model.ecommerce.AdmPolizasWs
import com.kotlinmark43.sockets.model.ecommerce.PanelAdmPolizasWs
import com.kotlinmark43.sockets.model.ecommerce.PanelSolicitudesWs
import com.kotlinmark43.sockets.model.ecommerce.SolicitudesWs
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/solicitudes-ws")
class SolicitudesWsController @Autowired
constructor( private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/putSolicitudesWs"], method = arrayOf(RequestMethod.PUT))
    fun greeting(@RequestBody message: SolicitudesWs): PanelSolicitudesWs? {
        try {
            val psws = PanelSolicitudesWs(message.id, message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.estado)
            this.template.convertAndSend("/task/panelSolicitudesWs/", psws)
            return psws
        } catch (e: Exception) {
            logger.error("Error al actualizar")
            e.printStackTrace()
            return null
        }
    }

    @MessageMapping("/putSolicitudesWs")
    @SendTo("/task/panelSolicitudesWs/")
    fun greetingRes(@RequestBody msg: SolicitudesWs): PanelSolicitudesWs? {
        try {
            val ps = PanelSolicitudesWs(msg.id, msg.nombre, msg.apellidoPaterno, msg.apellidoMaterno, msg.estado)
            return ps
        } catch (e: Exception) {
            SolicitudesWsController.logger.error("Error al actualizar")
            e.printStackTrace()
            return null
        }
    }

    companion object {
        private val logger = LogManager.getLogger(SolicitudesWsController::class.java)
    }
}