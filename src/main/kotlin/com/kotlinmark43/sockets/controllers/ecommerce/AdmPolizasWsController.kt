package com.kotlinmark43.sockets.controllers.ecommerce

import com.kotlinmark43.sockets.controllers.finanzas.PorAplicarController
import com.kotlinmark43.sockets.model.ecommerce.AdmPolizasWs
import com.kotlinmark43.sockets.model.ecommerce.PanelAdmPolizasWs
import com.kotlinmark43.sockets.model.ecommerce.registro.PanelRegistroAdminWs
import com.kotlinmark43.sockets.model.ecommerce.registro.RegistroAdminWs
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/adm-polizas-ws")
class AdmPolizasWsController @Autowired
constructor( private val template: SimpMessagingTemplate ) {
    /********************************************Actualiza un registro*************************************************/
    @RequestMapping(value = ["/putAdmPolizasWs"], method = arrayOf(RequestMethod.PUT))
    fun greeting(@RequestBody message: AdmPolizasWs): PanelAdmPolizasWs? {
        try {
            val p = PanelAdmPolizasWs(message.id, message.idEstadoRecibo, message.estado, message.archivoSubido)
            println("/task/panelAdmPolizasWs/" + message.id + message.idEstadoRecibo)
            this.template.convertAndSend("/task/panelAdmPolizasWs/", p)
            return p
        } catch (e: Exception) {
            logger.error("Error al actualizar")
            e.printStackTrace()
            return null
        }
    }

    @MessageMapping("/putAdmPolizasWs")
    @SendTo("/task/panelAdmPolizasWs/")
    fun greetingRes(@RequestBody msg: AdmPolizasWs): PanelAdmPolizasWs? {
        try {
            val p = PanelAdmPolizasWs(msg.id, msg.idEstadoRecibo, msg.estado, msg.archivoSubido)
            return p
        } catch (e: Exception) {
            AdmPolizasWsController.logger.error("Error al actualizar")
            e.printStackTrace()
            return null
        }
    }
    /********************************************Crea un registro*************************************************/
    @RequestMapping(value = ["/postRegistroAdmPolizasWs"], method = arrayOf(RequestMethod.POST))
    fun greetingPost(@RequestBody message: RegistroAdminWs): PanelRegistroAdminWs? {
        try {
            val p = PanelRegistroAdminWs(message.id, message.idFlujoPoliza, message.idEstadoRecibo, message.poliza,
                    message.nombreCliente, message.apellidoPaternoCliente, message.apellidoMaternoCliente, message.nombre,
                    message.apellidoPaterno, message.apellidoMaterno, message.nombreComercial, message.subarea,
                    message.estado, message.estadoRecibo, message.fechaInicio, message.numeroSerie, message.idRecibo,
                    message.idCliente, message.archivoSubido, 1)
            println("/task/panelRegistroAdmPolizasWs/" + "/" + message.id + "/" +  message.poliza + "/" + message.idTipoPeticion)
            this.template.convertAndSend("/task/panelRegistroAdmPolizasWs/", p)
            return p
        } catch (e: Exception) {
            logger.error("Error al actualizar")
            e.printStackTrace()
            return null
        }
    }

    @MessageMapping("/postRegistroAdmPolizasWs")
    @SendTo("/task/panelRegistroAdmPolizasWs/")
    fun greetingRegPost(@RequestBody msg: RegistroAdminWs): PanelRegistroAdminWs? {
        try {
            val p = PanelRegistroAdminWs(msg.id, msg.idFlujoPoliza, msg.idEstadoRecibo, msg.poliza,
                    msg.nombreCliente, msg.apellidoPaternoCliente, msg.apellidoMaternoCliente, msg.nombre,
                    msg.apellidoPaterno, msg.apellidoMaterno, msg.nombreComercial, msg.subarea,
                    msg.estado, msg.estadoRecibo, msg.fechaInicio, msg.numeroSerie, msg.idRecibo,
                    msg.idCliente, msg.archivoSubido, 1)
            return p
        } catch (e: Exception) {
            AdmPolizasWsController.logger.error("Error al actualizar")
            e.printStackTrace()
            return null
        }
    }

    companion object {
        private val logger = LogManager.getLogger(AdmPolizasWsController::class.java)
    }
}