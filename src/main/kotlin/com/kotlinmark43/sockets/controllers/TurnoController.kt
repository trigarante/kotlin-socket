package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Turnos
import com.kotlinmark43.sockets.model.PanelTurnos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/turno")
class TurnoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTurnos"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Turnos): PanelTurnos? {
        try {
            val p = PanelTurnos(message.nombre)
            this.template.convertAndSend("/task/panelTurnos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTurno")
    @SendTo("/task/panelTurnos")
    fun greeting1(@RequestBody v: Turnos): PanelTurnos? {
        try {
            val p = PanelTurnos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(TurnoController::class.java)
    }
}

