package com.kotlinmark43.sockets.controllers.marketing.catalogos

import com.kotlinmark43.sockets.model.marketing.catalogos.ProvedoresLeads
import com.kotlinmark43.sockets.model.marketing.catalogos.PanelProvedoresLeads
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/provedoresleads")
class ProvedoresLeadsController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveProvedoresLeads"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: ProvedoresLeads): PanelProvedoresLeads? {
        try {
            val p = PanelProvedoresLeads(message.nombre)
            this.template.convertAndSend("/task/panelProvedoresLeads", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveProvedoresLeads")
    @SendTo("/task/panelProvedoresLeads")
    fun greeting1(@RequestBody v: ProvedoresLeads): PanelProvedoresLeads? {
        try {
            val p = PanelProvedoresLeads(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(ProvedoresLeadsController::class.java)
    }
}

