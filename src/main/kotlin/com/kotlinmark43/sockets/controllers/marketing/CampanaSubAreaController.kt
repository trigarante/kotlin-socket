package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.CampanaSubArea
import com.kotlinmark43.sockets.model.marketing.PanelCampanaSubArea
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/campaniasubarea")
class CampanaSubAreaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCampaniaSubArea"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: CampanaSubArea): PanelCampanaSubArea? {
        try {
            val p = PanelCampanaSubArea(message.id,
                    message.idCampania,
                    message.idSubarea,
                    message.descripcion,
                    message.activo,
                    message.idSubRamo)
            this.template.convertAndSend("/task/PanelCampaniaSubArea", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCampaniaSubArea")
    @SendTo("/task/PanelCampaniaSubArea")
    fun greeting1(@RequestBody v: CampanaSubArea): PanelCampanaSubArea? {
        try {
            val p = PanelCampanaSubArea(v.id,
                    v.idCampania,
                    v.idSubarea,
                    v.descripcion,
                    v.activo,
                    v.idSubRamo)
            logger.info(p.id)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CampanaSubAreaController::class.java)
    }
}

