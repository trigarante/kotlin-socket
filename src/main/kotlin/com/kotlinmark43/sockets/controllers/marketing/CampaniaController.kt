package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.Campania
import com.kotlinmark43.sockets.model.marketing.PanelCampania
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/campania")
class CampaniaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCampania"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Campania): PanelCampania? {
        try {
            val p = PanelCampania(message.id,message.idSubRamo,message.nombre,message.descripcion,message.activo,message.idEstadoCampania)
            this.template.convertAndSend("/task/panelCampania", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCampania")
    @SendTo("/task/panelCampania")
    fun greeting1(@RequestBody v: Campania): PanelCampania? {
        try {
            val p = PanelCampania(v.id,v.idSubRamo,v.nombre,v.descripcion,v.activo,v.idEstadoCampania)
            logger.info(p.id)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CampaniaController::class.java)
    }
}

