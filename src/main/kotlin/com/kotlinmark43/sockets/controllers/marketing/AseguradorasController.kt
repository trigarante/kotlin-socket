package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.Aseguradoras
import com.kotlinmark43.sockets.model.marketing.PanelAseguradoras
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/aseguradoras")
class AseguradorasController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveAseguradoras"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Aseguradoras): PanelAseguradoras? {
        try {
            val p = PanelAseguradoras(message.nombre)
            this.template.convertAndSend("/task/panelAseguradoras", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveAseguradoras")
    @SendTo("/task/panelAseguradoras")
    fun greeting1(@RequestBody v: Aseguradoras): PanelAseguradoras? {
        try {
            val p = PanelAseguradoras(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(AseguradorasController::class.java)
    }
}

