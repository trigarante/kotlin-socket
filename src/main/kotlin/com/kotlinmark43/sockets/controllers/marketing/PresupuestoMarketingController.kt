package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.PresupuestoMarketing
import com.kotlinmark43.sockets.model.marketing.PanelPresupuestoMarketing
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/presupuestomarketing")
class PresupuestoMarketingController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/savePresupuestoMarketing"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: PresupuestoMarketing): PanelPresupuestoMarketing? {
        try {
            val p = PanelPresupuestoMarketing(message.id,
                    message.cantidad,
                    message.mesyanio,
                    message.activo)
            this.template.convertAndSend("/task/PanelPresupuestoMarketing", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/savePresupuestoMarketing")
    @SendTo("/task/PanelPresupuestoMarketing")
    fun greeting1(@RequestBody v: PresupuestoMarketing): PanelPresupuestoMarketing? {
        try {
            val p = PanelPresupuestoMarketing(v.id,
                    v.cantidad,
                    v.mesyanio,
                    v.activo)
            logger.info(p.id)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(PresupuestoMarketingController::class.java)
    }
}

