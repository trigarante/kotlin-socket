package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.DatosNodo
import com.kotlinmark43.sockets.model.marketing.PanelDatosNodo
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/datosnodo")
class DatosNodoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveDatosNodo"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: DatosNodo): PanelDatosNodo? {
        try {
            val p = PanelDatosNodo(message.nombre)
            this.template.convertAndSend("/task/panelDatosNodo", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveDatosNodo")
    @SendTo("/task/panelDatosNodo")
    fun greeting1(@RequestBody v: DatosNodo): PanelDatosNodo? {
        try {
            val p = PanelDatosNodo(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(DatosNodoController::class.java)
    }
}

