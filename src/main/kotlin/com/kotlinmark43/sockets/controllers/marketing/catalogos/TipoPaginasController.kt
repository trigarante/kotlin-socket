package com.kotlinmark43.sockets.controllers.marketing.catalogos

import com.kotlinmark43.sockets.model.marketing.catalogos.TipoPaginas
import com.kotlinmark43.sockets.model.marketing.catalogos.PanelTipoPaginas
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/tipopaginas")
class TipoPaginasController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTipoPaginas"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TipoPaginas): PanelTipoPaginas? {
        try {
            val p = PanelTipoPaginas(message.id,
                    message.tipo,
                    message.objetivo,
                    message.activo)
            this.template.convertAndSend("/task/panelTipoPaginas", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTipoPaginas")
    @SendTo("/task/panelTipoPaginas")
    fun greeting1(@RequestBody v: TipoPaginas): PanelTipoPaginas? {
        try {
            val p = PanelTipoPaginas(v.id,
                    v.tipo,
                    v.objetivo,
                    v.activo)
            logger.info(p.id)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(TipoPaginasController::class.java)
    }
}

