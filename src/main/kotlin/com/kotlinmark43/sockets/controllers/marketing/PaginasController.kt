package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.Paginas
import com.kotlinmark43.sockets.model.marketing.PanelPaginas
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/paginas")
class PaginasController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/savePaginas"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Paginas): PanelPaginas? {
        try {
            val p = PanelPaginas(message.id,
                    message.idTipoPagina,
                    message.idCampania,
                    message.url,
                    message.descripcion,
                    message.fechaRegistro,
                    message.fechaActualizado,
                    message.activo,
                    message.idSubArea)
            this.template.convertAndSend("/task/panelPaginas", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/savePaginas")
    @SendTo("/task/panelPaginas")
    fun greeting1(@RequestBody v: Paginas): PanelPaginas? {
        try {
            val p = PanelPaginas(v.id,
                    v.idTipoPagina,
                    v.idCampania,
                    v.url,
                    v.descripcion,
                    v.fechaRegistro,
                    v.fechaActualizado,
                    v.activo,
                    v.idSubArea)
            logger.info(p.id)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(PaginasController::class.java)
    }
}

