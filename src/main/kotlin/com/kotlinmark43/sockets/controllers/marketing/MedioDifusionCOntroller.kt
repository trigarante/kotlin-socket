package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.MedioDifusion
import com.kotlinmark43.sockets.model.marketing.PanelMedioDifusion
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/mediodifusion")
class MedioDifusionController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveMedioDifusion"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: MedioDifusion): PanelMedioDifusion? {
        try {
            val p = PanelMedioDifusion(message.id,
                    message.idSubarea,
                    message.idProveedor,
                    message.idExterno,
                    message.idPagina,
                    message.idPresupuesto,
                    message.descripcion,
                    message.configuracion,
                    message.activo)
            this.template.convertAndSend("/task/panelMedioDifusion", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveMedioDifusion")
    @SendTo("/task/panelMedioDifusion")
    fun greeting1(@RequestBody v: MedioDifusion): PanelMedioDifusion? {
        try {
            val p = PanelMedioDifusion(v.id,
                    v.idSubarea,
                    v.idProveedor,
                    v.idExterno,
                    v.idPagina,
                    v.idPresupuesto,
                    v.descripcion,
                    v.configuracion,
                    v.activo)
            logger.info(p.id)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(MedioDifusionController::class.java)
    }
}

