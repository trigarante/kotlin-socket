package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.EstadoCampania
import com.kotlinmark43.sockets.model.marketing.PanelEstadoCampania
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/estadocampania")
class EstadoCampaniaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveEstadoCampania"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: EstadoCampania): PanelEstadoCampania? {
        try {
            val p = PanelEstadoCampania(message.id,
                    message.estado,
                    message.activo)
            this.template.convertAndSend("/task/PanelEstadoCampania", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveEstadoCampania")
    @SendTo("/task/PanelEstadoCampania")
    fun greeting1(@RequestBody v: EstadoCampania): PanelEstadoCampania? {
        try {
            val p = PanelEstadoCampania(v.id,
                    v.estado,
                    v.activo)
            logger.info(p.id)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(EstadoCampaniaController::class.java)
    }
}

