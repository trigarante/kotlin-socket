package com.kotlinmark43.sockets.controllers.marketing

import com.kotlinmark43.sockets.model.marketing.CampaniaCategoria
import com.kotlinmark43.sockets.model.marketing.PanelCampaniaCategoria
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/campaniacategoria")
class CampaniaCategoriaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCampaniaCategoria"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: CampaniaCategoria): PanelCampaniaCategoria? {
        try {
            val p = PanelCampaniaCategoria(message.nombre)
            this.template.convertAndSend("/task/panelCampaniaCategoria", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCampaniaCategoria")
    @SendTo("/task/panelCampaniaCategoria")
    fun greeting1(@RequestBody v: CampaniaCategoria): PanelCampaniaCategoria? {
        try {
            val p = PanelCampaniaCategoria(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CampaniaCategoriaController::class.java)
    }
}

