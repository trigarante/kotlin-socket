package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelSocios
import com.kotlinmark43.sockets.model.Socios
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/socios")
class SociosController @Autowired

constructor(private val template: SimpMessagingTemplate) {
  @RequestMapping(value = ["/saveSocios"], method = arrayOf(RequestMethod.POST))
  fun greeting(@RequestBody message: Socios): PanelSocios? {
    try {
      val p = PanelSocios( message.idEstadoSocio, message.prioridad, message.nombreComercial,
        message.rfc, message.razonSocial, message.alias, message.prioridades, message.estado, message.activo )
      this.template.convertAndSend("/task/panelSocios", p)
      return p
    }
    catch (e: Exception) {
      logger.error("Error al insertar")
      e.printStackTrace()
      return null
    }
  }

//  Response
  @MessageMapping("/saveSocios")  @SendTo("/task/panelSocios")
  fun greeting1(@RequestBody v: Socios): PanelSocios? {
    try {
      val p = PanelSocios(v.idEstadoSocio, v.prioridad, v.nombreComercial,
        v.rfc, v.razonSocial, v.alias, v.prioridades, v.estado, v.activo)
      logger.info(p.activo, p.idEstadoSocio, p.alias, p.nombreComercial, p.prioridad, p.razonSocial, p.rfc)
      logger.info(p.alias)

      return p
    } catch (e: Exception) {
      logger.error("Error al insertar")
      e.printStackTrace()
      return null
    }
  }
  companion object {
    private val logger = LogManager.getLogger(SociosController::class.java)
  }
}

