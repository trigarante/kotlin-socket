package com.kotlinmark43.sockets.controllers.ventaNueva
import com.kotlinmark43.sockets.model.ventaNueva.PanelVnProductoCliente
import com.kotlinmark43.sockets.model.ventaNueva.VnProductoCliente
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnProductoCliente")
class VnProductoClienteController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnProductoCliente"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnProductoCliente): PanelVnProductoCliente? {
        try {
            val p = PanelVnProductoCliente (message.nombre)
            this.template.convertAndSend("/task/panelVnProductoCliente", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnProductoCliente")
    @SendTo("/task/panelVnProductoCliente")
    fun greeting1(@RequestBody v: VnProductoCliente): PanelVnProductoCliente? {
        try {
            val p = PanelVnProductoCliente(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnProductoClienteController::class.java)
    }
}