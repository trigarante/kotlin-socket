package com.kotlinmark43.sockets.controllers.ventaNueva
import com.kotlinmark43.sockets.model.ventaNueva.PanelVnRecibos
import com.kotlinmark43.sockets.model.ventaNueva.VnRecibos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnRecibos")
class VnRecibosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnRecibos"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnRecibos): PanelVnRecibos? {
        try {
            val p = PanelVnRecibos (message.nombre)
            this.template.convertAndSend("/task/panelVnRecibos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnRecibos")
    @SendTo("/task/panelVnRecibos")
    fun greeting1(@RequestBody v: VnRecibos): PanelVnRecibos? {
        try {
            val p = PanelVnRecibos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnRecibosController::class.java)
    }
}