package com.kotlinmark43.sockets.controllers.ventaNueva
import com.kotlinmark43.sockets.model.ventaNueva.PanelVnRegistro
import com.kotlinmark43.sockets.model.ventaNueva.VnRegistro
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnRegistro")
class VnRegistroController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnRegistro"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnRegistro): PanelVnRegistro? {
        try {
            val p = PanelVnRegistro (message.nombre)
            this.template.convertAndSend("/task/panelVnRegistro", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnRegistro")
    @SendTo("/task/panelVnRegistro")
    fun greeting1(@RequestBody v: VnRegistro): PanelVnRegistro? {
        try {
            val p = PanelVnRegistro(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnRegistroController::class.java)
    }
}