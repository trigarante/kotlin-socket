package com.kotlinmark43.sockets.controllers.ventaNueva.catalogos

import com.kotlinmark43.sockets.model.ventaNueva.catalogos.PanelVnTipoContacto
import com.kotlinmark43.sockets.model.ventaNueva.catalogos.VnTipoContacto
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vntipocontacto")
class VnTipoContactoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnTipoContacto"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnTipoContacto): PanelVnTipoContacto? {
        try {
            val p = PanelVnTipoContacto (message.nombre)
            this.template.convertAndSend("/task/panelVnTipoContacto", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnTipoContacto")
    @SendTo("/task/panelVnTipoContacto")
    fun greeting1(@RequestBody v: VnTipoContacto): PanelVnTipoContacto? {
        try {
            val p = PanelVnTipoContacto(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnTipoContactoController::class.java)
    }
}




