package com.kotlinmark43.sockets.controllers.ventaNueva

import com.kotlinmark43.sockets.model.ventaNueva.Solicitudes
import com.kotlinmark43.sockets.model.ventaNueva.PanelSolicitudes
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnsolicitudes")
class VnSolicitudesController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    // Cuando insertan una nueva solicitud
    @RequestMapping(value = ["/saveVnSolicitudes"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Solicitudes): PanelSolicitudes? {
        try {
            val p = PanelSolicitudes(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 1)
            this.template.convertAndSend("/task/panelVnSolicitudes/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Cuando actualizan el estado de la solicitud
    @RequestMapping(value = ["/updateEstadoSolicitud"], method = arrayOf(RequestMethod.PUT))
    fun updateEstadoSolicitud(@RequestBody message: Solicitudes): PanelSolicitudes? {
        try {
            val p = PanelSolicitudes(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 2)
            println("/task/panelVnSolicitudes/" + message.tipoSolicitud)
            this.template.convertAndSend("/task/panelVnSolicitudes/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Cuando reasignan la solicitud
        @RequestMapping(value = ["/updateEmpleado"], method = arrayOf(RequestMethod.PUT))
    fun updateEmpleado(@RequestBody message: Solicitudes): PanelSolicitudes? {
        try {
            val p = PanelSolicitudes(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 3)
            println("/task/panelVnSolicitudes/" + message.tipoSolicitud)
            this.template.convertAndSend("/task/panelVnSolicitudes/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Cuando acabas inspeccion para quitar la solicitud
    @RequestMapping(value = ["/removeSolicitud"], method = arrayOf(RequestMethod.PUT))
    fun removeSolicitud(@RequestBody message: Solicitudes): PanelSolicitudes? {
        try {
            val p = PanelSolicitudes(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 4)
            println("/task/panelVnSolicitudes/" + message.tipoSolicitud)
            this.template.convertAndSend("/task/panelVnSolicitudes/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnSolicitudes")
    @SendTo("/task/panelVnSolicitudes")
    fun greeting1(@RequestBody message: Solicitudes): PanelSolicitudes? {
        try {
            val p = PanelSolicitudes(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 1)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(VnSolicitudesController::class.java)
    }
}

