package com.kotlinmark43.sockets.controllers.ventaNueva
import com.kotlinmark43.sockets.model.ventaNueva.PanelVnPagos
import com.kotlinmark43.sockets.model.ventaNueva.VnPagos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnPagos")
class VnPagosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnPagos"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnPagos): PanelVnPagos? {
        try {
            val p = PanelVnPagos (message.nombre)
            this.template.convertAndSend("/task/panelVnPagos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnPagos")
    @SendTo("/task/panelVnPagos")
    fun greeting1(@RequestBody v: VnPagos): PanelVnPagos? {
        try {
            val p = PanelVnPagos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnPagosController::class.java)
    }
}