package com.kotlinmark43.sockets.controllers.ventaNueva
import com.kotlinmark43.sockets.model.ventaNueva.PanelVnInspecciones
import com.kotlinmark43.sockets.model.ventaNueva.VnInspecciones
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnInspecciones")
class VnInspeccionesController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnInspecciones"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnInspecciones): PanelVnInspecciones? {
        try {
            val p = PanelVnInspecciones (message.nombre)
            this.template.convertAndSend("/task/panelVnInspecciones", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnInspecciones")
    @SendTo("/task/panelVnInspecciones")
    fun greeting1(@RequestBody v: VnInspecciones): PanelVnInspecciones? {
        try {
            val p = PanelVnInspecciones(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnInspeccionesController::class.java)
    }
}