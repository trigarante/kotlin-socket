package com.kotlinmark43.sockets.controllers.ventaNueva

import com.kotlinmark43.sockets.model.ventaNueva.PanelVnClientes
import com.kotlinmark43.sockets.model.ventaNueva.VnClientes
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnClientes")
class VnClientesController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnClientes"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnClientes): PanelVnClientes? {
        try {
            val p = PanelVnClientes (message.nombre)
            this.template.convertAndSend("/task/panelVnClientes", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnClientes")
    @SendTo("/task/panelVnClientes")
    fun greeting1(@RequestBody v: VnClientes): PanelVnClientes? {
        try {
            val p = PanelVnClientes(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnClientesController::class.java)
    }
}