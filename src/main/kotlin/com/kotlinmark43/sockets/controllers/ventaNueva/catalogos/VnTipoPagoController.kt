package com.kotlinmark43.sockets.controllers.ventaNueva.catalogos

import com.kotlinmark43.sockets.model.ventaNueva.catalogos.PanelVnTipoPago
import com.kotlinmark43.sockets.model.ventaNueva.catalogos.VnTipoPago
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vntipopago")
class VnTipoPagoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnTipoPago"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnTipoPago): PanelVnTipoPago? {
        try {
            val p = PanelVnTipoPago (message.nombre)
            this.template.convertAndSend("/task/panelVnTipoPago", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnTipoPago")
    @SendTo("/task/panelVnTipoPago")
    fun greeting1(@RequestBody v: VnTipoPago): PanelVnTipoPago? {
        try {
            val p = PanelVnTipoPago(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnTipoPagoController::class.java)
    }
}




