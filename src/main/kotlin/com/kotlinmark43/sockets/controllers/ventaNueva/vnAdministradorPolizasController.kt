package com.kotlinmark43.sockets.controllers.ventaNueva

import com.kotlinmark43.sockets.model.ventaNueva.AdministradorPolizas
import com.kotlinmark43.sockets.model.ventaNueva.PanelAdministradorPolizas
import com.kotlinmark43.sockets.model.ventaNueva.PanelSolicitudes
import com.kotlinmark43.sockets.model.ventaNueva.Solicitudes
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnAdministradorPolizas")
class vnAdministradorPolizasController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    // Cuando insertan una nueva poliza
    @RequestMapping(value = ["/saveVnPoliza"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: AdministradorPolizas): PanelAdministradorPolizas? {
        try {
            val p = PanelAdministradorPolizas(message.id, message.idFlujoPoliza, message.idEstadoRecibo, message.poliza,
                    message.nombreCliente, message.apellidoPaternoCliente, message.apellidoMaternoCliente, message.nombre, message.apellidoPaterno,
                    message.apellidoMaterno, message.nombreComercial, message.subarea, message.estado, message.estadoRecibo,
                    message.fechaInicio, message.numeroSerie, message.idRecibo, message.idCliente, message.archivoSubido, message.idPago, message.idEstadoPago,
                    1)
            this.template.convertAndSend("/task/panelVnAdministradorPolizas/", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Update CLiente in steps
    @RequestMapping(value = ["/updateCliente"], method = arrayOf(RequestMethod.PUT))
    fun updateCliente(@RequestBody message: AdministradorPolizas): PanelAdministradorPolizas? {
        try {
            val p = PanelAdministradorPolizas(message.id, message.idFlujoPoliza, message.idEstadoRecibo, message.poliza,
                    message.nombreCliente, message.apellidoPaternoCliente, message.apellidoMaternoCliente, message.nombre, message.apellidoPaterno,
                    message.apellidoMaterno, message.nombreComercial, message.subarea, message.estado, message.estadoRecibo,
                    message.fechaInicio, message.numeroSerie, message.idRecibo, message.idCliente, message.archivoSubido, message.idPago, message.idEstadoPago,
                    2)
            this.template.convertAndSend("/task/panelVnAdministradorPolizas/", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Update Numero Serie
    @RequestMapping(value = ["/updateNumeroSerie"], method = arrayOf(RequestMethod.PUT))
    fun updateNumeroSerie(@RequestBody message: AdministradorPolizas): PanelAdministradorPolizas? {
        try {
            val p = PanelAdministradorPolizas(message.id, message.idFlujoPoliza, message.idEstadoRecibo, message.poliza,
                    message.nombreCliente, message.apellidoPaternoCliente, message.apellidoMaternoCliente, message.nombre, message.apellidoPaterno,
                    message.apellidoMaterno, message.nombreComercial, message.subarea, message.estado, message.estadoRecibo,
                    message.fechaInicio, message.numeroSerie, message.idRecibo, message.idCliente, message.archivoSubido, message.idPago, message.idEstadoPago,
                    3)
            this.template.convertAndSend("/task/panelVnAdministradorPolizas/", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Update Archivo Subido
    @RequestMapping(value = ["/updateArchivoSubido"], method = arrayOf(RequestMethod.PUT))
    fun updateArchivoSubido(@RequestBody message: AdministradorPolizas): PanelAdministradorPolizas? {
        try {
            val p = PanelAdministradorPolizas(message.id, message.idFlujoPoliza, message.idEstadoRecibo, message.poliza,
                    message.nombreCliente, message.apellidoPaternoCliente, message.apellidoMaternoCliente, message.nombre, message.apellidoPaterno,
                    message.apellidoMaterno, message.nombreComercial, message.subarea, message.estado, message.estadoRecibo,
                    message.fechaInicio, message.numeroSerie, message.idRecibo, message.idCliente, message.archivoSubido, message.idPago, message.idEstadoPago,
                    4)
            this.template.convertAndSend("/task/panelVnAdministradorPolizas/", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Update Pago
    @RequestMapping(value = ["/updatePago"], method = arrayOf(RequestMethod.PUT))
    fun updatePago(@RequestBody message: AdministradorPolizas): PanelAdministradorPolizas? {
        try {
            val p = PanelAdministradorPolizas(message.id, message.idFlujoPoliza, message.idEstadoRecibo, message.poliza,
                    message.nombreCliente, message.apellidoPaternoCliente, message.apellidoMaternoCliente, message.nombre, message.apellidoPaterno,
                    message.apellidoMaterno, message.nombreComercial, message.subarea, message.estado, message.estadoRecibo,
                    message.fechaInicio, message.numeroSerie, message.idRecibo, message.idCliente, message.archivoSubido, message.idPago, message.idEstadoPago,
                    5)
            this.template.convertAndSend("/task/panelVnAdministradorPolizas/", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Update Estado Poliza
    @RequestMapping(value = ["/updateEstadoPoliza"], method = arrayOf(RequestMethod.PUT))
    fun updateEstadoPoliza(@RequestBody message: AdministradorPolizas): PanelAdministradorPolizas? {
        try {
            val p = PanelAdministradorPolizas(message.id, message.idFlujoPoliza, message.idEstadoRecibo, message.poliza,
                    message.nombreCliente, message.apellidoPaternoCliente, message.apellidoMaternoCliente, message.nombre, message.apellidoPaterno,
                    message.apellidoMaterno, message.nombreComercial, message.subarea, message.estado, message.estadoRecibo,
                    message.fechaInicio, message.numeroSerie, message.idRecibo, message.idCliente, message.archivoSubido, message.idPago, message.idEstadoPago,
                    6)
            this.template.convertAndSend("/task/panelVnAdministradorPolizas/", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }


    companion object {

        private val logger = LogManager.getLogger(vnAdministradorPolizasController::class.java)
    }

}