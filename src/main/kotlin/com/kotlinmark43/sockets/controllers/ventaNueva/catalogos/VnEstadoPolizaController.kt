package com.kotlinmark43.sockets.controllers.ventaNueva.catalogos

import com.kotlinmark43.sockets.model.ventaNueva.catalogos.PanelVnEstadoPoliza
import com.kotlinmark43.sockets.model.ventaNueva.catalogos.VnEstadoPoliza
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vnestadopoliza")
class VnEstadoPolizaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveVnEstadoPoliza"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: VnEstadoPoliza): PanelVnEstadoPoliza? {
        try {
            val p = PanelVnEstadoPoliza (message.nombre)
            this.template.convertAndSend("/task/panelVnEstadoPoliza", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveVnEstadoPoliza")
    @SendTo("/task/panelVnEstadoPoliza")
    fun greeting1(@RequestBody v: VnEstadoPoliza): PanelVnEstadoPoliza? {
        try {
            val p = PanelVnEstadoPoliza(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(VnEstadoPolizaController::class.java)
    }
}




