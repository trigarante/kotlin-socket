package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Contrato
import com.kotlinmark43.sockets.model.PanelContrato
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/contratos")
class ContratoController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveContrato/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Contrato) : PanelContrato? {
        try {
            val p = PanelContrato(mensaje.fechaInicio, mensaje.fechaFin, mensaje.activo)
            this.template.convertAndSend("/task/panelContratos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveContrato/1")  @SendTo("/task/panelContratos")
    fun greeting1(@RequestBody v: Contrato): PanelContrato? {
        try {
            val p = PanelContrato(v.fechaInicio, v.fechaFin, v.activo)
            logger.info(p.fechaInicio)
            return p
        }
        catch (e: Exception) {
            ContratoController.logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(ContratoController ::class.java)
    }
}