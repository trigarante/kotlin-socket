package com.kotlinmark43.sockets.controllers


import com.kotlinmark43.sockets.model.PanelSubRamo
import com.kotlinmark43.sockets.model.SubRamo
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/subramo")
class SubRamoController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveSubRamo/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: SubRamo): PanelSubRamo? {
        try {
            val p = PanelSubRamo(mensaje.descripcion, mensaje.activo, mensaje.prioridad )
            this.template.convertAndSend("/task/panelSubRamo", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveSubRamo/1")  @SendTo("/task/panelSubRamo")
    fun greeting1(@RequestBody v: SubRamo): PanelSubRamo? {
        try {
            val p = PanelSubRamo(v.descripcion, v.activo, v.prioridad)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(RamoController::class.java)
    }
}