package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelBancos
import com.kotlinmark43.sockets.model.Bancos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bancos")
class BancosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBanco"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Bancos): PanelBancos? {
        try {
            val p = PanelBancos(message.nombre)
            this.template.convertAndSend("/task/panelBancos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBanco")
    @SendTo("/task/panelBancos")
    fun greeting1(@RequestBody v: Bancos): PanelBancos? {
        try {
            val p = PanelBancos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BancosController::class.java)
    }
}

