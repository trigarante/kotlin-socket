package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelMotivosBaja
import com.kotlinmark43.sockets.model.MotivosBaja
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/motivosbaja")
class MotivosBajaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveMotivosBaja"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: MotivosBaja): PanelMotivosBaja? {
        try {
            val p = PanelMotivosBaja(message.nombre)
            this.template.convertAndSend("/task/panelMotivosBaja", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveMotivosBaja")
    @SendTo("/task/panelMotivosBaja")
    fun greeting1(@RequestBody v: MotivosBaja): PanelMotivosBaja? {
        try {
            val p = PanelMotivosBaja(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(MotivosBajaController::class.java)
    }
}

