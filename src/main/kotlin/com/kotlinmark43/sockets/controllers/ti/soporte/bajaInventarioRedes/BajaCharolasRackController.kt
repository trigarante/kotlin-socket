package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioRedes

import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PanelCharolasRack
import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.CharolasRack
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajacharolasrack")
class BajaCharolasRackController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaCharolasRack"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: CharolasRack): PanelCharolasRack? {
        try {
            val p = PanelCharolasRack(message.nombre)
            this.template.convertAndSend("/task/panelBajaCharolasRack", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaCharolasRack")
    @SendTo("/task/panelBajaCharolasRack")
    fun greeting1(@RequestBody v: CharolasRack): PanelCharolasRack? {
        try {
            val p = PanelCharolasRack(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaCharolasRackController::class.java)
    }
}

