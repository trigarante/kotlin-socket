package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelBiometricos
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.Biometricos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/biometricos")
class BiometricosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBiometricos"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Biometricos): PanelBiometricos? {
        try {
            val p = PanelBiometricos(message.nombre)
            this.template.convertAndSend("/task/panelBiometricos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBiometricos")
    @SendTo("/task/panelBiometricos")
    fun greeting1(@RequestBody v: Biometricos): PanelBiometricos? {
        try {
            val p = PanelBiometricos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BiometricosController::class.java)
    }
}

