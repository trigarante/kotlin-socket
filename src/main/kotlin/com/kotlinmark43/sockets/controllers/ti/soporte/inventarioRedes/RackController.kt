package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioRedes

import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PanelRack
import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.Rack
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/rack")
class RackController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveRack"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Rack): PanelRack? {
        try {
            val p = PanelRack(message.nombre)
            this.template.convertAndSend("/task/panelRack", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveRack")
    @SendTo("/task/panelRack")
    fun greeting1(@RequestBody v: Rack): PanelRack? {
        try {
            val p = PanelRack(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(RackController::class.java)
    }
}

