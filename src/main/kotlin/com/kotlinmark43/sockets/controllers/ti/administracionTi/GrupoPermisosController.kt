package com.kotlinmark43.sockets.controllers.ti.administracionTi

import com.kotlinmark43.sockets.model.ti.administracionTI.GrupoPermisos
import com.kotlinmark43.sockets.model.ti.administracionTI.PanelGrupoPermisos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/grupopermisos")
class GrupoPermisosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveGrupoPermisos"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: GrupoPermisos): PanelGrupoPermisos? {
        try {
            val p = PanelGrupoPermisos(message.nombre)
            this.template.convertAndSend("/task/panelGrupoPermisos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveGrupoPermisos")
    @SendTo("/task/panelGrupoPermisos")
    fun greeting1(@RequestBody v: GrupoPermisos): PanelGrupoPermisos? {
        try {
            val p = PanelGrupoPermisos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(GrupoPermisosController::class.java)
    }
}