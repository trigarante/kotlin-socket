package com.kotlinmark43.sockets.controllers.ti.administracionTi

import com.kotlinmark43.sockets.model.ti.administracionTI.Extensiones
import com.kotlinmark43.sockets.model.ti.administracionTI.PanelExtensiones
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/extensiones")
class ExtensionesController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveExtensiones"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Extensiones): PanelExtensiones? {
        try {
            val p = PanelExtensiones(message.nombre)
            this.template.convertAndSend("/task/panelExtensiones", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveExtensiones")
    @SendTo("/task/panelExtensiones")
    fun greeting1(@RequestBody v: Extensiones): PanelExtensiones? {
        try {
            val p = PanelExtensiones(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(ExtensionesController::class.java)
    }
}

