package com.kotlinmark43.sockets.controllers.ti.administracionTi

import com.kotlinmark43.sockets.model.ti.administracionTI.PanelReactivarPermisos
import com.kotlinmark43.sockets.model.ti.administracionTI.ReactivarPermisos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/reactivarpermisos")
class ReactivarPermisosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveReactivarPermisos"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: ReactivarPermisos): PanelReactivarPermisos? {
        try {
            val p = PanelReactivarPermisos(message.nombre)
            this.template.convertAndSend("/task/panelReactivarPermisos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveReactivarPermisos")
    @SendTo("/task/panelReactivarPermisos")
    fun greeting1(@RequestBody v: ReactivarPermisos): PanelReactivarPermisos? {
        try {
            val p = PanelReactivarPermisos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(ReactivarPermisosController::class.java)
    }
}

