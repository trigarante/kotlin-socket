package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioUsuario

import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.PanelLaptop
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.Laptop
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajalaptop")
class BajaLaptopController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaLaptop"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Laptop): PanelLaptop? {
        try {
            val p = PanelLaptop(message.nombre)
            this.template.convertAndSend("/task/panelBajaLaptop", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaLaptop")
    @SendTo("/task/panelBajaLaptop")
    fun greeting1(@RequestBody v: Laptop): PanelLaptop? {
        try {
            val p = PanelLaptop(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaLaptopController::class.java)
    }
}

