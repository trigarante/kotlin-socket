package com.kotlinmark43.sockets.controllers.ti.administracionTi

import com.kotlinmark43.sockets.model.ti.administracionTI.PanelTiUsuariosLicencias
import com.kotlinmark43.sockets.model.ti.administracionTI.TiUsuariosLicencias
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/tiusuarioslicencias")
class TiUsuariosLicenciasController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTiUsuariosLicencias"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TiUsuariosLicencias): PanelTiUsuariosLicencias? {
        try {
            val p = PanelTiUsuariosLicencias (message.nombre)
            this.template.convertAndSend("/task/panelTiUsuariosLicencias", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTiUsuariosLicencias")
    @SendTo("/task/panelTiUsuariosLicencias")
    fun greeting1(@RequestBody v: TiUsuariosLicencias): PanelTiUsuariosLicencias? {
        try {
            val p = PanelTiUsuariosLicencias(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(TiUsuariosLicenciasController::class.java)
    }
}


