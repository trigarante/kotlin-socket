package com.kotlinmark43.sockets.controllers.ti.soporte.catalogos

import com.kotlinmark43.sockets.model.ti.soporte.catalogos.EstadoInventario
import com.kotlinmark43.sockets.model.ti.soporte.catalogos.PanelEstadoInventario
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/estadoinventario")
class EstadoInventarioController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveEstadoInventario"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: EstadoInventario): PanelEstadoInventario? {
        try {
            val p = PanelEstadoInventario(message.nombre)
            this.template.convertAndSend("/task/panelEstadoInventario", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveEstadoInventario")
    @SendTo("/task/panelEstadoInventario")
    fun greeting1(@RequestBody v: EstadoInventario): PanelEstadoInventario? {
        try {
            val p = PanelEstadoInventario(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(EstadoInventarioController::class.java)
    }
}

