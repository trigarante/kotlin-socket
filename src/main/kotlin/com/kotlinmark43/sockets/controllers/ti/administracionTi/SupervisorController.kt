package com.kotlinmark43.sockets.controllers.ti.administracionTi

import com.kotlinmark43.sockets.model.ti.administracionTI.PanelSupervisor
import com.kotlinmark43.sockets.model.ti.administracionTI.Supervisor
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/supervisor")
class SupervisorController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveSupervisor"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Supervisor): PanelSupervisor? {
        try {
            val p = PanelSupervisor (message.nombre)
            this.template.convertAndSend("/task/panelSupervisor", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveSupervisor")
    @SendTo("/task/panelSupervisor")
    fun greeting1(@RequestBody v: Supervisor): PanelSupervisor? {
        try {
            val p = PanelSupervisor(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(SupervisorController::class.java)
    }
}


