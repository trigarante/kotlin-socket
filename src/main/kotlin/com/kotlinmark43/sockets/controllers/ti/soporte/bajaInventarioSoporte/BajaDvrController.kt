package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelDvr
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.Dvr
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajadvr")
class BajaDvrController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaDvr"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Dvr): PanelDvr? {
        try {
            val p = PanelDvr(message.nombre)
            this.template.convertAndSend("/task/panelBajaDvr", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaDvr")
    @SendTo("/task/panelBajaDvr")
    fun greeting1(@RequestBody v: Dvr): PanelDvr? {
        try {
            val p = PanelDvr(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaDvrController::class.java)
    }
}

