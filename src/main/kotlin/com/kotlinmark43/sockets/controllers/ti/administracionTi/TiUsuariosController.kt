package com.kotlinmark43.sockets.controllers.ti.administracionTi

import com.kotlinmark43.sockets.model.ti.administracionTI.PanelTiUsuarios
import com.kotlinmark43.sockets.model.ti.administracionTI.TiUsuarios
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/tiusuarios")
class TiUsuariosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTiUsuarios"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TiUsuarios): PanelTiUsuarios? {
        try {
            val p = PanelTiUsuarios (message.nombre)
            this.template.convertAndSend("/task/panelTiUsuarios", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTiUsuarios")
    @SendTo("/task/panelTiUsuarios")
    fun greeting1(@RequestBody v: TiUsuarios): PanelTiUsuarios? {
        try {
            val p = PanelTiUsuarios(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(TiUsuariosController::class.java)
    }
}


