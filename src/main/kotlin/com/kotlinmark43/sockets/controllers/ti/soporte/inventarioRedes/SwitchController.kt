package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioRedes

import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PanelSwitch
import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.Switch
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/switch")
class SwitchController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveSwitch"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Switch): PanelSwitch? {
        try {
            val p = PanelSwitch(message.nombre)
            this.template.convertAndSend("/task/panelSwitch", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveSwitch")
    @SendTo("/task/panelSwitch")
    fun greeting1(@RequestBody v: Switch): PanelSwitch? {
        try {
            val p = PanelSwitch(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(SwitchController::class.java)
    }
}

