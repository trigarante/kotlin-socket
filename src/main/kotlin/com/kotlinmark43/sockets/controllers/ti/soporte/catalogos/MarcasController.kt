package com.kotlinmark43.sockets.controllers.ti.soporte.catalogos

import com.kotlinmark43.sockets.model.ti.soporte.catalogos.Marcas
import com.kotlinmark43.sockets.model.ti.soporte.catalogos.PanelMarcas
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/soportemarcas")
class MarcasController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveSoporteMarcas"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Marcas): PanelMarcas? {
        try {
            val p = PanelMarcas(message.nombre)
            this.template.convertAndSend("/task/panelSoporteMarcas", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveSoporteMarcas")
    @SendTo("/task/panelSoporteMarcas")
    fun greeting1(@RequestBody v: Marcas): PanelMarcas? {
        try {
            val p = PanelMarcas(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(MarcasController::class.java)
    }
}

