package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelBiometricos
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.Biometricos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajabiometricos")
class BajaBiometricosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaBiometricos"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Biometricos): PanelBiometricos? {
        try {
            val p = PanelBiometricos(message.nombre)
            this.template.convertAndSend("/task/panelBajaBiometricos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaBiometricos")
    @SendTo("/task/panelBajaBiometricos")
    fun greeting1(@RequestBody v: Biometricos): PanelBiometricos? {
        try {
            val p = PanelBiometricos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaBiometricosController::class.java)
    }
}

