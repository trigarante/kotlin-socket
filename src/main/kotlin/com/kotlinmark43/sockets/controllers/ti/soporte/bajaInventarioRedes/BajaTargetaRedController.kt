package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioRedes

import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PanelTargetaRed
import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.TargetaRed
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajatargetared")
class BajaTargetaRedController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaTargetaRed"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TargetaRed): PanelTargetaRed? {
        try {
            val p = PanelTargetaRed(message.nombre)
            this.template.convertAndSend("/task/panelBajaTargetaRed", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaTargetaRed")
    @SendTo("/task/panelBajaTargetaRed")
    fun greeting1(@RequestBody v: TargetaRed): PanelTargetaRed? {
        try {
            val p = PanelTargetaRed(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaTargetaRedController::class.java)
    }
}

