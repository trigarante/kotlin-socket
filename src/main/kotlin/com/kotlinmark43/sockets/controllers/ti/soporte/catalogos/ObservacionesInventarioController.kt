package com.kotlinmark43.sockets.controllers.ti.soporte.catalogos
import com.kotlinmark43.sockets.model.ti.soporte.catalogos.ObservacionesInventario
import com.kotlinmark43.sockets.model.ti.soporte.catalogos.PanelObservacionesInventario
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/observacionesinventario")
class ObservacionesInventarioController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveObservacionesInventario"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: ObservacionesInventario): PanelObservacionesInventario? {
        try {
            val p = PanelObservacionesInventario(message.nombre)
            this.template.convertAndSend("/task/panelObservacionesInventario", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveObservacionesInventario")
    @SendTo("/task/panelObservacionesInventario")
    fun greeting1(@RequestBody v: ObservacionesInventario): PanelObservacionesInventario? {
        try {
            val p = PanelObservacionesInventario(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(ObservacionesInventarioController::class.java)
    }
}

