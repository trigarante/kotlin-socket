package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioUsuario

import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.PanelDiadema
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.Diadema
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/diadema")
class DiademaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveDiadema"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Diadema): PanelDiadema? {
        try {
            val p = PanelDiadema(message.nombre)
            this.template.convertAndSend("/task/panelDiadema", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveDiadema")
    @SendTo("/task/panelDiadema")
    fun greeting1(@RequestBody v: Diadema): PanelDiadema? {
        try {
            val p = PanelDiadema(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(DiademaController::class.java)
    }
}

