package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioUsuario
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.Cpu
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.PanelCpu
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajacpu")
class BajaCpuController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaCpu"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Cpu): PanelCpu? {
        try {
            val p = PanelCpu(message.nombre)
            this.template.convertAndSend("/task/panelBajaCpu", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaCpu")
    @SendTo("/task/panelBajaCpu")
    fun greeting1(@RequestBody v: Cpu): PanelCpu? {
        try {
            val p = PanelCpu(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaCpuController::class.java)
    }
}

