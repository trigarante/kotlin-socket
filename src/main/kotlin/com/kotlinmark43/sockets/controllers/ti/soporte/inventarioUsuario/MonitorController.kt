package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioUsuario

import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.PanelMonitor
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.Monitor
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/monitor")
class MonitorController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveMonitor"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Monitor): PanelMonitor? {
        try {
            val p = PanelMonitor(message.nombre)
            this.template.convertAndSend("/task/panelMonitor", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveMonitor")
    @SendTo("/task/panelMonitor")
    fun greeting1(@RequestBody v: Monitor): PanelMonitor? {
        try {
            val p = PanelMonitor(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(MonitorController::class.java)
    }
}

