package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelCorrienteCamara
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.CorrienteCamara
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/corrientecamara")
class CorrienteCamaraController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCorrienteCamara"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: CorrienteCamara): PanelCorrienteCamara? {
        try {
            val p = PanelCorrienteCamara(message.nombre)
            this.template.convertAndSend("/task/panelCorrienteCamara", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCorrienteCamara")
    @SendTo("/task/panelCorrienteCamara")
    fun greeting1(@RequestBody v: CorrienteCamara): PanelCorrienteCamara? {
        try {
            val p = PanelCorrienteCamara(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CorrienteCamaraController::class.java)
    }
}

