package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioRedes

import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PanelPatchPanel
import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PatchPanel
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajapatchpanel")
class BajaPatchPanelController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaPatchPanel"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: PatchPanel): PanelPatchPanel? {
        try {
            val p = PanelPatchPanel(message.nombre)
            this.template.convertAndSend("/task/panelBajaPatchPanel", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaPatchPanel")
    @SendTo("/task/panelBajaPatchPanel")
    fun greeting1(@RequestBody v: PatchPanel): PanelPatchPanel? {
        try {
            val p = PanelPatchPanel(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaPatchPanelController::class.java)
    }
}

