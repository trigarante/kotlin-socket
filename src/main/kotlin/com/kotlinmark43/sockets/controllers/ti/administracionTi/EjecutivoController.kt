package com.kotlinmark43.sockets.controllers.ti.administracionTi

import com.kotlinmark43.sockets.model.ti.administracionTI.Ejecutivo
import com.kotlinmark43.sockets.model.ti.administracionTI.PanelEjecutivo
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/ejecutivo")
class EjecutivoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveEjecutivo"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Ejecutivo): PanelEjecutivo? {
        try {
            val p = PanelEjecutivo(message.nombre)
            this.template.convertAndSend("/task/panelEjecutivo", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveEjecutivo")
    @SendTo("/task/panelEjecutivo")
    fun greeting1(@RequestBody v: Ejecutivo): PanelEjecutivo? {
        try {
            val p = PanelEjecutivo(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(EjecutivoController::class.java)
    }
}


