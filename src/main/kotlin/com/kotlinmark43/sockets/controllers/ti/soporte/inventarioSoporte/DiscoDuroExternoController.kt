package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelDiscoDuroExterno
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.DiscoDuroExterno
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/discoduroexterno")
class DiscoDuroExternoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveDiscoDuroExterno"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: DiscoDuroExterno): PanelDiscoDuroExterno? {
        try {
            val p = PanelDiscoDuroExterno(message.nombre)
            this.template.convertAndSend("/task/panelDiscoDuroExterno", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveDiscoDuroExterno")
    @SendTo("/task/panelDiscoDuroExterno")
    fun greeting1(@RequestBody v: DiscoDuroExterno): PanelDiscoDuroExterno? {
        try {
            val p = PanelDiscoDuroExterno(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(DiscoDuroExternoController::class.java)
    }
}

