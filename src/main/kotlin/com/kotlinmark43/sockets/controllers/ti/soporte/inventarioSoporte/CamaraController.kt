package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelCamara
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.Camara
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/camara")
class CamaraController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCamara"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Camara): PanelCamara? {
        try {
            val p = PanelCamara(message.nombre)
            this.template.convertAndSend("/task/panelCamara", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCamara")
    @SendTo("/task/panelCamara")
    fun greeting1(@RequestBody v: Camara): PanelCamara? {
        try {
            val p = PanelCamara(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CamaraController::class.java)
    }
}

