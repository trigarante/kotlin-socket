package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelChromeBit
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.ChromeBit
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/chromebit")
class ChromeBitController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveChromeBit"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: ChromeBit): PanelChromeBit? {
        try {
            val p = PanelChromeBit(message.nombre)
            this.template.convertAndSend("/task/panelChromeBit", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveChromeBit")
    @SendTo("/task/panelChromeBit")
    fun greeting1(@RequestBody v: ChromeBit): PanelChromeBit? {
        try {
            val p = PanelChromeBit(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(ChromeBitController::class.java)
    }
}

