package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioRedes

import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PanelTelefonoIP
import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.TelefonoIP
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/telefonoip")
class TelefonoIPController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTelefonoIP"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TelefonoIP): PanelTelefonoIP? {
        try {
            val p = PanelTelefonoIP(message.nombre)
            this.template.convertAndSend("/task/panelTelefonoIP", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTelefonoIP")
    @SendTo("/task/panelTelefonoIP")
    fun greeting1(@RequestBody v: TelefonoIP): PanelTelefonoIP? {
        try {
            val p = PanelTelefonoIP(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(TelefonoIPController::class.java)
    }
}

