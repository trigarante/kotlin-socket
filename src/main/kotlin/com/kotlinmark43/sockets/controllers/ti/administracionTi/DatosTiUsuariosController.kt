package com.kotlinmark43.sockets.controllers.ti.administracionTi

import com.kotlinmark43.sockets.model.ti.administracionTI.DatosTiUsuarios
import com.kotlinmark43.sockets.model.ti.administracionTI.PanelDatosTiUsuarios
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/datostiusuarios")
class DatosTiUsuariosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveDatosTiUsuarios"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: DatosTiUsuarios): PanelDatosTiUsuarios? {
        try {
            val p = PanelDatosTiUsuarios (message.nombre)
            this.template.convertAndSend("/task/panelDatosTiUsuarios", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveDatosTiUsuarios")
    @SendTo("/task/panelDatosTiUsuarios")
    fun greeting1(@RequestBody v: DatosTiUsuarios): PanelDatosTiUsuarios? {
        try {
            val p = PanelDatosTiUsuarios(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(DatosTiUsuariosController::class.java)
    }
}


