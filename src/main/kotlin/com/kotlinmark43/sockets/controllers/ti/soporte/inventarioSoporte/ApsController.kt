package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelAps
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.Aps
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/aps")
class ApsController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveAps"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Aps): PanelAps? {
        try {
            val p = PanelAps(message.nombre)
            this.template.convertAndSend("/task/panelAps", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveAps")
    @SendTo("/task/panelAps")
    fun greeting1(@RequestBody v: Aps): PanelAps? {
        try {
            val p = PanelAps(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(ApsController::class.java)
    }
}

