package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioRedes

import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PanelServidores
import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.Servidores
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajaservidores")
class BajaServidoresController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaServidores"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Servidores): PanelServidores? {
        try {
            val p = PanelServidores(message.nombre)
            this.template.convertAndSend("/task/panelBajaServidores", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaServidores")
    @SendTo("/task/panelBajaServidores")
    fun greeting1(@RequestBody v: Servidores): PanelServidores? {
        try {
            val p = PanelServidores(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaServidoresController::class.java)
    }
}

