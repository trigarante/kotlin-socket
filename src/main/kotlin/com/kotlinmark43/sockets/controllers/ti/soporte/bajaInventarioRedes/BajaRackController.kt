package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioRedes

import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.PanelRack
import com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes.Rack
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajarack")
class BajaRackController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaRack"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Rack): PanelRack? {
        try {
            val p = PanelRack(message.nombre)
            this.template.convertAndSend("/task/panelBajaRack", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaRack")
    @SendTo("/task/panelBajaRack")
    fun greeting1(@RequestBody v: Rack): PanelRack? {
        try {
            val p = PanelRack(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaRackController::class.java)
    }
}

