package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioUsuario

import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.PanelTeclado
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.Teclado
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/teclado")
class TecladoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTeclado"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Teclado): PanelTeclado? {
        try {
            val p = PanelTeclado(message.nombre)
            this.template.convertAndSend("/task/panelTeclado", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTeclado")
    @SendTo("/task/panelTeclado")
    fun greeting1(@RequestBody v: Teclado): PanelTeclado? {
        try {
            val p = PanelTeclado(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(TecladoController::class.java)
    }
}

