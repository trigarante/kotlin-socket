package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioUsuario

import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.PanelTablet
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.Tablet
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajatablet")
class BajaTabletController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaTablet"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Tablet): PanelTablet? {
        try {
            val p = PanelTablet(message.nombre)
            this.template.convertAndSend("/task/panelBajaTablet", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaTablet")
    @SendTo("/task/panelBajaTablet")
    fun greeting1(@RequestBody v: Tablet): PanelTablet? {
        try {
            val p = PanelTablet(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaTabletController::class.java)
    }
}

