package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioUsuario

import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.PanelMouse
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.Mouse
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajamouse")
class BajaMouseController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaMouse"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Mouse): PanelMouse? {
        try {
            val p = PanelMouse(message.nombre)
            this.template.convertAndSend("/task/panelBajaMouse", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaMouse")
    @SendTo("/task/panelBajaMouse")
    fun greeting1(@RequestBody v: Mouse): PanelMouse? {
        try {
            val p = PanelMouse(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaMouseController::class.java)
    }
}

