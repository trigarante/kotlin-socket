package com.kotlinmark43.sockets.controllers.ti.soporte.inventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelBarraMultiContacto
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.BarraMultiContacto
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/barramulticontacto")
class BarraMultiConttactoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBarraMultiContacto"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: BarraMultiContacto): PanelBarraMultiContacto? {
        try {
            val p = PanelBarraMultiContacto(message.nombre)
            this.template.convertAndSend("/task/panelBarraMultiContacto", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBarraMultiContacto")
    @SendTo("/task/panelBarraMultiContacto")
    fun greeting1(@RequestBody v: BarraMultiContacto): PanelBarraMultiContacto? {
        try {
            val p = PanelBarraMultiContacto(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BarraMultiConttactoController::class.java)
    }
}

