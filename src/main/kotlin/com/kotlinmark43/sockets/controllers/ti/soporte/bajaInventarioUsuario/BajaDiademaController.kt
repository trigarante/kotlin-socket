package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioUsuario

import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.PanelDiadema
import com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario.Diadema
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajadiadema")
class BajaDiademaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaDiadema"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Diadema): PanelDiadema? {
        try {
            val p = PanelDiadema(message.nombre)
            this.template.convertAndSend("/task/panelBajaDiadema", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaDiadema")
    @SendTo("/task/panelBajaDiadema")
    fun greeting1(@RequestBody v: Diadema): PanelDiadema? {
        try {
            val p = PanelDiadema(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaDiademaController::class.java)
    }
}

