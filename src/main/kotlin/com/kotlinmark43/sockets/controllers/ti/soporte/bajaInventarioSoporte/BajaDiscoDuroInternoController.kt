package com.kotlinmark43.sockets.controllers.ti.soporte.bajaInventarioSoporte

import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.PanelDiscoDuroInterno
import com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte.DiscoDuroInterno
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajadiscodurointerno")
class BajaDiscoDuroInternoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaDiscoDuroInterno"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: DiscoDuroInterno): PanelDiscoDuroInterno? {
        try {
            val p = PanelDiscoDuroInterno(message.nombre)
            this.template.convertAndSend("/task/panelBajaDiscoDuroInterno", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaDiscoDuroInterno")
    @SendTo("/task/panelBajaDiscoDuroInterno")
    fun greeting1(@RequestBody v: DiscoDuroInterno): PanelDiscoDuroInterno? {
        try {
            val p = PanelDiscoDuroInterno(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaDiscoDuroInternoController::class.java)
    }
}

