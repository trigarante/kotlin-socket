package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelEstadoEscolaridad
import com.kotlinmark43.sockets.model.EstadoEscolaridad
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/estadoescolaridad")
class EstadoEscolaridadController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveEstadoEscolaridad"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: EstadoEscolaridad): PanelEstadoEscolaridad? {
        try {
            val p = PanelEstadoEscolaridad(message.nombre)
            this.template.convertAndSend("/task/panelEstadoEscolaridad", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveEstadoEscolaridad")
    @SendTo("/task/panelEstadoEscolaridad")
    fun greeting1(@RequestBody v: EstadoEscolaridad): PanelEstadoEscolaridad? {
        try {
            val p = PanelEstadoEscolaridad(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(EstadoEscolaridadController::class.java)
    }
}

