package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Convenio
import com.kotlinmark43.sockets.model.PanelConvenio
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/convenios")
class ConvenioController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveConvenio/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Convenio) : PanelConvenio? {
        try {
            val p = PanelConvenio(mensaje.cantidad, mensaje.activo)
            this.template.convertAndSend("/task/panelConvenios", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveConvenio/1")  @SendTo("/task/panelConvenios")
    fun greeting1(@RequestBody v: Convenio): PanelConvenio? {
        try {
            val p = PanelConvenio(v.cantidad, v.activo)
            logger.info(p.activo)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(ConvenioController::class.java)
    }
}