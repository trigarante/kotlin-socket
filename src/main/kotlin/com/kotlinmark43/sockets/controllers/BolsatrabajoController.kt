package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelBolsatrabajo
import com.kotlinmark43.sockets.model.Bolsatrabajo
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bolsatrabajo")
class BolsatrabajoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBolsaTrabajo"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Bolsatrabajo): PanelBolsatrabajo? {
        try {
            val p = PanelBolsatrabajo(message.nombre)
            this.template.convertAndSend("/task/panelBolsaTrabajo", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBolsaTrabajo")
    @SendTo("/task/panelBolsaTrabajo")
    fun greeting1(@RequestBody v: Bolsatrabajo): PanelBolsatrabajo? {
        try {
            val p = PanelBolsatrabajo(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BolsatrabajoController::class.java)
    }
}

