package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Cobertura
import com.kotlinmark43.sockets.model.PanelCobertura

import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/cobertura")
class CoberturaController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCobertura/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Cobertura) : PanelCobertura? {
        try {
            val p = PanelCobertura(mensaje.detalle, mensaje.activo)
            this.template.convertAndSend("/task/panelCobertura", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveCobertura/1")  @SendTo("/task/panelCobertura")
    fun greeting1(@RequestBody v: Cobertura): PanelCobertura? {
        try {
            val p = PanelCobertura(v.detalle, v.activo)
            logger.info(p.detalle, p.activo)
            return p
        }
catch (e: Exception) {
    logger.error("Error al insertar")
    e.printStackTrace()
    return null
}
}
companion object {
    private val logger = LogManager.getLogger(CoberturaController::class.java)
}
}