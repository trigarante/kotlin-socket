package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelVacantes
import com.kotlinmark43.sockets.model.Vacantes
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/vacantes")
class VacantesController @Autowired
    constructor(private val template: SimpMessagingTemplate) {
        @RequestMapping(value = ["/saveVacante/1"], method = arrayOf(RequestMethod.POST))
        fun greeting(@RequestBody message: Vacantes): PanelVacantes? {
            try {
                val p = PanelVacantes( message.nombreVacante, message.grupos, message.empresa,
                        message.sede, message.area, message.subarea, message.puesto, message.tipoPuesto)
                this.template.convertAndSend("/task/panel/1", p)
                var prueba = "perro";
                    this.template.convertAndSend("/task/notificaciones/1", "Que paso pro")
                return p
            }
            catch (e: Exception) {
                logger.error("Error al insertar")
                e.printStackTrace()
                return null
            }
        }


        //Response
        @MessageMapping("/saveVacante/1")
        @SendTo("/task/panel/1")
        fun greeting1(@RequestBody v: Vacantes): PanelVacantes? {
            try {
                val p = PanelVacantes(v.nombreVacante, v.grupos, v.empresa,
                        v.sede, v.area, v.subarea, v.puesto, v.tipoPuesto)
                logger.info(p.nombreVacante, p.grupos, p.empresa, p.sede, p.area, p.subarea, p.puesto)
                logger.info(p.nombreVacante)

                return p
            } catch (e: Exception) {
                logger.error("Error al insertar")
                e.printStackTrace()
                return null
            }
        }
        companion object {

            private val logger = LogManager.getLogger(VacantesController::class.java)
        }
    }
