package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.NotificacionesUsuario
import com.kotlinmark43.sockets.model.PanelVacantes
import com.kotlinmark43.sockets.model.Vacantes
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/notificaciones")
class NotificacionesUsuarioController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/addNotification"], method = arrayOf(RequestMethod.GET))
    fun greeting() {
        try {
            this.template.convertAndSend("/task/addNotification", "Se inserto un nuevo registro")
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
        }
    }

    //Response
    @MessageMapping("/saveNotificacion/1")
    @SendTo("/task/panel/1")
    fun greeting1(@RequestBody v: Vacantes) {

    }
    companion object {

        private val logger = LogManager.getLogger(NotificacionesUsuarioController::class.java)
    }
}
