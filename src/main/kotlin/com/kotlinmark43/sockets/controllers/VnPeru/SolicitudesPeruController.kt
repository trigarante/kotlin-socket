package com.kotlinmark43.sockets.controllers.VnPeru

import com.kotlinmark43.sockets.model.VnPeru.PanelSolicitudesPeru
import com.kotlinmark43.sockets.model.VnPeru.SolicitudesPeru
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.apache.logging.log4j.LogManager

@Controller
@CrossOrigin
@RestController
@RequestMapping("/VnPeruSolicitudes")
class SolicitudesPeruController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    // Cuando insertan una nueva solicitud
    @RequestMapping(value = ["/saveVnSolicitudes"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: SolicitudesPeru): PanelSolicitudesPeru? {
        try {
            val p = PanelSolicitudesPeru(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 1)
            this.template.convertAndSend("/task/panelVnSolicitudesPeru/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Cuando actualizan el estado de la solicitud
    @RequestMapping(value = ["/updateEstadoSolicitud"], method = arrayOf(RequestMethod.PUT))
    fun updateEstadoSolicitud(@RequestBody message: SolicitudesPeru): PanelSolicitudesPeru? {
        try {
            val p = PanelSolicitudesPeru(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 2)
            this.template.convertAndSend("/task/panelVnSolicitudesPeru/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Cuando reasignan la solicitud
    @RequestMapping(value = ["/updateEmpleado"], method = arrayOf(RequestMethod.PUT))
    fun updateEmpleado(@RequestBody message: SolicitudesPeru): PanelSolicitudesPeru? {
        try {
            val p = PanelSolicitudesPeru(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 3)
            this.template.convertAndSend("/task/panelVnSolicitudesPeru/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Cuando acabas inspeccion para quitar la solicitud
    @RequestMapping(value = ["/removeSolicitud"], method = arrayOf(RequestMethod.PUT))
    fun removeSolicitud(@RequestBody message: SolicitudesPeru): PanelSolicitudesPeru? {
        try {
            val p = PanelSolicitudesPeru(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 4)
            this.template.convertAndSend("/task/panelVnSolicitudesPeru/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    // Cuando actualizas el prospecto
    @RequestMapping(value = ["/updateProspecto"], method = arrayOf(RequestMethod.PUT))
    fun updateProspecto(@RequestBody message: SolicitudesPeru): PanelSolicitudesPeru? {
        try {
            val p = PanelSolicitudesPeru(message.id, message.idCotizacionAli, message.idEmpleado, message.idEstadoSolicitud,
                    message.fechaSolicitud, message.estado, message.idSubArea, message.subArea, message.idTipoContacto,
                    message.nombre, message.apellidoPaterno, message.apellidoMaterno, message.idProducto, message.idProspecto,
                    message.nombreProspecto, message.numeroProspecto, message.etiquetaSolicitud, 5)
            this.template.convertAndSend("/task/panelVnSolicitudesPeru/" + message.tipoSolicitud, p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    companion object {

        private val logger = LogManager.getLogger(SolicitudesPeruController::class.java)
    }
}
