package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelPreempleados
import com.kotlinmark43.sockets.model.Preempleados
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/preempleados")
class PreempleadosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/savePreempleado"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Preempleados): PanelPreempleados? {
        try {
            val p = PanelPreempleados(message.calificacionRollPlay, message.comentariosFaseDos, message.idEtapa)
            this.template.convertAndSend("/task/panelPreempleados", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/savePreempleado")
    @SendTo("/task/panelPreempleados")
    fun greeting1(@RequestBody v: Preempleados): PanelPreempleados? {
        try {
            val p = PanelPreempleados(v.calificacionRollPlay, v.comentariosFaseDos, v.idEtapa)
            logger.info(p.calificacionRollPlay, p.comentariosFaseDos, p.idEtapa)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(PreempleadosController::class.java)
    }
}

