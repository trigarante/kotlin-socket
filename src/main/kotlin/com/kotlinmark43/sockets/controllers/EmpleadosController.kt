package com.kotlinmark43.sockets.controllers
import com.kotlinmark43.sockets.model.PanelEmpleados
import com.kotlinmark43.sockets.model.Empleados
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/empleados")
class EmpleadosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveEmpleado"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Empleados): PanelEmpleados? {
        try {
            val p = PanelEmpleados(message.id, message.idCandidato, message.idPuesto, message.fechaIngreso, message.puestoDetalle, message.comentarios)
            this.template.convertAndSend("/task/panelEmpleados", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveEmpleado")
    @SendTo("/task/panelEmpleados")
    fun greeting1(@RequestBody v: Empleados): PanelEmpleados? {
        try {
            val p = PanelEmpleados(v.id, v.idCandidato, v.idPuesto, v.fechaIngreso, v.puestoDetalle, v.comentarios)
            logger.info(p)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(EmpleadosController::class.java)
    }
}

