package com.kotlinmark43.sockets.controllers.comercial.catalogos

import com.kotlinmark43.sockets.model.comercial.catalogos.PanelTipoCobertura
import com.kotlinmark43.sockets.model.comercial.catalogos.TipoCobertura
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/tipocobertura")
class TipoCoberturaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTipoCobertura"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TipoCobertura): PanelTipoCobertura? {
        try {
            val p = PanelTipoCobertura(message.nombre)
            this.template.convertAndSend("/task/panelTipoCobertura", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTipoCobertura")
    @SendTo("/task/panelTipoCobertura")
    fun greeting1(@RequestBody v: TipoCobertura): PanelTipoCobertura? {
        try {
            val p = PanelTipoCobertura(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(TipoCoberturaController::class.java)
    }
}

