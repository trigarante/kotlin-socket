package com.kotlinmark43.sockets.controllers.comercial.catalogos

import com.kotlinmark43.sockets.model.comercial.catalogos.PanelTipoCompetencia
import com.kotlinmark43.sockets.model.comercial.catalogos.TipoCompetencia
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/tipocompetencia")
class TipoCompetenciaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTipoCompetencia"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TipoCompetencia): PanelTipoCompetencia? {
        try {
            val p = PanelTipoCompetencia(message.nombre)
            this.template.convertAndSend("/task/panelTipoCompetencia", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTipoCompetencia")
    @SendTo("/task/panelTipoCompetencia")
    fun greeting1(@RequestBody v: TipoCompetencia): PanelTipoCompetencia? {
        try {
            val p = PanelTipoCompetencia(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(TipoCompetenciaController::class.java)
    }
}

