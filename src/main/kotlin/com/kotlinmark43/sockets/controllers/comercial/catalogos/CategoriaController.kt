package com.kotlinmark43.sockets.controllers.comercial.catalogos

import com.kotlinmark43.sockets.model.comercial.catalogos.PanelCategoria
import com.kotlinmark43.sockets.model.comercial.catalogos.Categoria
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/categoria")
class CategoriaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCategoria"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Categoria): PanelCategoria? {
        try {
            val p = PanelCategoria(message.nombre)
            this.template.convertAndSend("/task/panelCategoria", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCategoria")
    @SendTo("/task/panelCategoria")
    fun greeting1(@RequestBody v: Categoria): PanelCategoria? {
        try {
            val p = PanelCategoria(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CategoriaController::class.java)
    }
}

