package com.kotlinmark43.sockets.controllers.comercial.catalogos

import com.kotlinmark43.sockets.model.comercial.catalogos.PanelDivisas
import com.kotlinmark43.sockets.model.comercial.catalogos.Divisas
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/divisas")
class DivisasController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveDivisas"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Divisas): PanelDivisas? {
        try {
            val p = PanelDivisas(message.nombre)
            this.template.convertAndSend("/task/panelDivisas", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveDivisas")
    @SendTo("/task/panelDivisas")
    fun greeting1(@RequestBody v: Divisas): PanelDivisas? {
        try {
            val p = PanelDivisas(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(DivisasController::class.java)
    }
}

