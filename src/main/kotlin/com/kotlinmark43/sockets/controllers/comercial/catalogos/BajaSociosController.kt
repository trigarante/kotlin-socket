package com.kotlinmark43.sockets.controllers.comercial.catalogos

import com.kotlinmark43.sockets.model.comercial.catalogos.PanelBajaSocios
import com.kotlinmark43.sockets.model.comercial.catalogos.BajaSocios
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajasocios")
class BajaSociosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajaSocio"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: BajaSocios): PanelBajaSocios? {
        try {
            val p = PanelBajaSocios(message.nombre)
            this.template.convertAndSend("/task/panelBajaSocios", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajaSocios")
    @SendTo("/task/panelBajaSocios")
    fun greeting1(@RequestBody v: BajaSocios): PanelBajaSocios? {
        try {
            val p = PanelBajaSocios(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajaSociosController::class.java)
    }
}

