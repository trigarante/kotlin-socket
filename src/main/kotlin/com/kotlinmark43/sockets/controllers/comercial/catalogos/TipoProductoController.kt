package com.kotlinmark43.sockets.controllers.comercial.catalogos

import com.kotlinmark43.sockets.model.comercial.catalogos.PanelTipoProducto
import com.kotlinmark43.sockets.model.comercial.catalogos.TipoProducto
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/tipoproducto")
class TipoProductoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTipoProducto"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TipoProducto): PanelTipoProducto? {
        try {
            val p = PanelTipoProducto(message.nombre)
            this.template.convertAndSend("/task/panelTipoProducto", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTipoProducto")
    @SendTo("/task/panelTipoProducto")
    fun greeting1(@RequestBody v: TipoProducto): PanelTipoProducto? {
        try {
            val p = PanelTipoProducto(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(TipoProductoController::class.java)
    }
}

