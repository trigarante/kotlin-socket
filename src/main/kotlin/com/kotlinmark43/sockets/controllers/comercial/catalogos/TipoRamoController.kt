package com.kotlinmark43.sockets.controllers.comercial.catalogos

import com.kotlinmark43.sockets.model.comercial.catalogos.PanelTipoRamo
import com.kotlinmark43.sockets.model.comercial.catalogos.TipoRamo
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/tiporamo")
class TipoRamoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveTipoRamo"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: TipoRamo): PanelTipoRamo? {
        try {
            val p = PanelTipoRamo(message.nombre)
            this.template.convertAndSend("/task/panelTipoRamo", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveTipoRamo")
    @SendTo("/task/panelTipoRamo")
    fun greeting1(@RequestBody v: TipoRamo): PanelTipoRamo? {
        try {
            val p = PanelTipoRamo(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(TipoRamoController::class.java)
    }
}

