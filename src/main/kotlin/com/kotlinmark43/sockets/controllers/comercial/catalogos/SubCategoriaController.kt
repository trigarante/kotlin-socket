package com.kotlinmark43.sockets.controllers.comercial.catalogos

import com.kotlinmark43.sockets.model.comercial.catalogos.PanelSubCategoria
import com.kotlinmark43.sockets.model.comercial.catalogos.SubCategoria
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/subcategoria")
class SubCategoriaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveSubCategoria"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: SubCategoria): PanelSubCategoria? {
        try {
            val p = PanelSubCategoria(message.nombre)
            this.template.convertAndSend("/task/panelSubCategoria", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveSubCategoria")
    @SendTo("/task/panelSubCategoria")
    fun greeting1(@RequestBody v: SubCategoria): PanelSubCategoria? {
        try {
            val p = PanelSubCategoria(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(SubCategoriaController::class.java)
    }
}

