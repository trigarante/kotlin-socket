package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelPrecandidatos
import com.kotlinmark43.sockets.model.Precandidatos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/precandidatos")
class PrecandidatosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/savePrecandidato/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Precandidatos): PanelPrecandidatos? {
        try {
            val p = PanelPrecandidatos(message.nombre, message.telefono, message.sede, message.subarea)
            this.template.convertAndSend("/task/panelPrecandidatos/1", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/savePrecandidato/1")
    @SendTo("/task/panelPrecandidatos/1")
    fun greeting1(@RequestBody v: Precandidatos): PanelPrecandidatos? {
        try {
            val p = PanelPrecandidatos(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(PrecandidatosController::class.java)
    }
}

