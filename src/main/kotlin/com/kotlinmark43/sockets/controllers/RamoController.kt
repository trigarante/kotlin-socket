package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelRamo
import com.kotlinmark43.sockets.model.Ramo
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/ramo")
class RamoController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveRamo/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Ramo): PanelRamo? {
        try {
            val p = PanelRamo(mensaje.descripcion, mensaje.activo, mensaje.prioridad )
            this.template.convertAndSend("/task/panelRamo", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveRamo/1")  @SendTo("/task/panelRamo")
    fun greeting1(@RequestBody v: Ramo): PanelRamo? {
        try {
            val p = PanelRamo(v.descripcion, v.activo, v.prioridad)
            logger.info(p.descripcion, p.activo, v.prioridad)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(RamoController::class.java)
    }
}