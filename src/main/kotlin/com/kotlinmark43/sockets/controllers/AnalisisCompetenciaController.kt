package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.AnalisisCompetencia
import com.kotlinmark43.sockets.model.PanelAnalisisCompetencia
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/analisis-competencia")
class AnalisisCompetenciaController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveACompetencia/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: AnalisisCompetencia) : PanelAnalisisCompetencia? {
        try {
            val p = PanelAnalisisCompetencia(mensaje.precios, mensaje.participacion, mensaje.activo)
            this.template.convertAndSend("/task/panelAnalisisCompetencia", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveACompetencia/1")  @SendTo("/task/panelAnalisisCompetencia")
    fun greeting1(@RequestBody v: AnalisisCompetencia): PanelAnalisisCompetencia? {
        try {
            val p = PanelAnalisisCompetencia(v.precios, v.participacion, v.activo)
            logger.info(p.activo)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(AnalisisCompetenciaController::class.java)
    }
}
