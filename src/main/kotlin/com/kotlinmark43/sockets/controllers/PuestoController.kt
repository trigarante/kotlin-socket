package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelPuesto
import com.kotlinmark43.sockets.model.Puesto
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/puesto")
class PuestoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/savePuesto"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Puesto): PanelPuesto? {
        try {
            val p = PanelPuesto(message.nombre)
            this.template.convertAndSend("/task/panelPuesto", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/savePuesto")
    @SendTo("/task/panelPuesto")
    fun greeting1(@RequestBody v: Puesto): PanelPuesto? {
        try {
            val p = PanelPuesto(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(PuestoController::class.java)
    }
}

