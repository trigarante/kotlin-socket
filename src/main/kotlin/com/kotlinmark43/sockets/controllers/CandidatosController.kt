package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Candidatos
import com.kotlinmark43.sockets.model.PanelCandidatos
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/candidato")
class CandidatosController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCandidato"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Candidatos): PanelCandidatos? {
        try {
            val p = PanelCandidatos( message.comentarios)
            this.template.convertAndSend("/task/panelCandidatos", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCandidato")
    @SendTo("/task/panelCandidatos")
    fun greeting1(@RequestBody v: Candidatos): PanelCandidatos? {
        try {
            val p = PanelCandidatos(v.calificacionPsicometrico, v.calificacionExamen, v.comentarios, v.idPrecandidato, v.fechaIngreso)
            logger.info(p)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CandidatosController::class.java)
    }
}