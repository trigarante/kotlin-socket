package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelUrl
import com.kotlinmark43.sockets.model.Url
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/url")
class UrlController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveUrl/1"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Url) : PanelUrl? {
        try {
            val p = PanelUrl(mensaje.url, mensaje.descripcion, mensaje.activo)
            this.template.convertAndSend("/task/panelUrl", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveUrl/1")  @SendTo("/task/panelUrl")
    fun greeting1(@RequestBody v: Url): PanelUrl? {
        try {
            val p = PanelUrl(v.url, v.descripcion, v.activo)
            logger.info(p.url, p.descripcion, p.activo)
            return p
        }
        catch (e: Exception) {
            UrlController.logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(UrlController::class.java)
    }
}