package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelEscolaridad
import com.kotlinmark43.sockets.model.Escolaridad
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/escolaridad")
class EscolaridadController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveEscolaridad"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: Escolaridad): PanelEscolaridad? {
        try {
            val p = PanelEscolaridad(message.nombre)
            this.template.convertAndSend("/task/panelEscolaridad", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveEscolaridad")
    @SendTo("/task/panelEscolaridad")
    fun greeting1(@RequestBody v: Escolaridad): PanelEscolaridad? {
        try {
            val p = PanelEscolaridad(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(EscolaridadController::class.java)
    }
}

