package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelMedioTransporte
import com.kotlinmark43.sockets.model.MedioTransporte
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/mediotransporte")
class MedioTransporteController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveMedioTransporte"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: MedioTransporte): PanelMedioTransporte? {
        try {
            val p = PanelMedioTransporte(message.nombre)
            this.template.convertAndSend("/task/panelMedioTransporte", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveMedioTransporte")
    @SendTo("/task/panelMedioTransporte")
    fun greeting1(@RequestBody v: MedioTransporte): PanelMedioTransporte? {
        try {
            val p = PanelMedioTransporte(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(MedioTransporteController::class.java)
    }
}

