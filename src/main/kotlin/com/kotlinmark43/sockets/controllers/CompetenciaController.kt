package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.Competencia
import com.kotlinmark43.sockets.model.PanelCompetencia
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/competencias")
class CompetenciaController @Autowired

constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCompetencia"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody mensaje: Competencia) : PanelCompetencia? {
        try {
            val p = PanelCompetencia(mensaje.competencia, mensaje.activo)
            this.template.convertAndSend("/task/panelCompetencias", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    //  Response
    @MessageMapping("/saveCompetencia")  @SendTo("/task/panelCompetencias")
    fun greeting1(@RequestBody v: Competencia): PanelCompetencia? {
        try {
            val p = PanelCompetencia(v.competencia, v.activo)
            logger.info(p.competencia, p.activo)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {
        private val logger = LogManager.getLogger(CompetenciaController::class.java)
    }
}