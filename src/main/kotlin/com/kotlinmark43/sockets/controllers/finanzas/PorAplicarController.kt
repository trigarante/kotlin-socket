package com.kotlinmark43.sockets.controllers.finanzas

import com.kotlinmark43.sockets.model.finanzas.PanelPorAplicar
import com.kotlinmark43.sockets.model.finanzas.PorAplicar
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/porAplicar")
class PorAplicarController @Autowired
constructor( private val template: SimpMessagingTemplate ) {
    @RequestMapping(value = ["/putPorAplicar"], method = arrayOf(RequestMethod.PUT))
    fun greeting(@RequestBody message: PorAplicar): PanelPorAplicar? {
         try {
             val p = PanelPorAplicar(message.id, message.idEstadoRecibo, message.poliza, message.nombreComercial, message.tipoSubRamo)
             println("/task/panelPorAplicar/" + message.id + message.idEstadoRecibo)
             this.template.convertAndSend("/task/panelPorAplicar/", p)
             return p
         } catch (e: Exception) {
             logger.error("Error al actualizar")
             e.printStackTrace()
             return null
         }
    }

    @MessageMapping("/putPorAplicar")
    @SendTo("/task/panelPorAplicar/")
    fun greeting1(@RequestBody v: PorAplicar): PanelPorAplicar? {
        try {
            val p = PanelPorAplicar(v.id, v.idEstadoRecibo, v.poliza, v.nombreComercial, v.tipoSubRamo)
            return p
        } catch (e: Exception) {
            logger.error("Error al actualizar")
            e.printStackTrace()
            return null
        }
    }

    companion object {
        private val logger = LogManager.getLogger(PorAplicarController::class.java)
    }
}