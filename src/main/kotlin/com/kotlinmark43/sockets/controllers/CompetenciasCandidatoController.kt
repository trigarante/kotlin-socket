package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelCompetenciasCandidato
import com.kotlinmark43.sockets.model.CompetenciasCandidato
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/competenciascandidato")
class CompetenciasCandidatoController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveCompetenciaCandidato"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: CompetenciasCandidato): PanelCompetenciasCandidato? {
        try {
            val p = PanelCompetenciasCandidato(message.nombre)
            this.template.convertAndSend("/task/panelCompetenciasCandidato", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveCompetenciaCandidato")
    @SendTo("/task/panelCompetenciasCandidato")
    fun greeting1(@RequestBody v: CompetenciasCandidato): PanelCompetenciasCandidato? {
        try {
            val p = PanelCompetenciasCandidato(v.nombre)
            logger.info(p.nombre)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(CompetenciasCandidatoController::class.java)
    }
}

