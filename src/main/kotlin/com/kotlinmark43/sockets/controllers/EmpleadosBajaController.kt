package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelEmpleadosBaja
import com.kotlinmark43.sockets.model.EmpleadosBaja
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/empleadosbaja")
class EmpleadosBajaController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveEmpleadoBaja"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: EmpleadosBaja): PanelEmpleadosBaja? {
        try {
            val p = PanelEmpleadosBaja(message.id, message.idCandidato, message.idPuesto, message.fechaIngreso, message.puestoDetalle, message.comentarios)
            this.template.convertAndSend("/task/panelEmpleadosBaja", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveEmpleadoBaja")
    @SendTo("/task/panelEmpleadosBaja")
    fun greeting1(@RequestBody v: EmpleadosBaja): PanelEmpleadosBaja? {
        try {
            val p = PanelEmpleadosBaja(v.id, v.idCandidato, v.idPuesto, v.fechaIngreso, v.puestoDetalle, v.comentarios)
            logger.info(p)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(EmpleadosBajaController::class.java)
    }
}

