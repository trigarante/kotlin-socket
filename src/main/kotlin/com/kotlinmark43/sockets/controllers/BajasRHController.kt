package com.kotlinmark43.sockets.controllers

import com.kotlinmark43.sockets.model.PanelBajasRH
import com.kotlinmark43.sockets.model.BajasRH
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@Controller
@CrossOrigin
@RestController
@RequestMapping("/bajasrh")
class BajasRHController @Autowired
constructor(private val template: SimpMessagingTemplate) {
    @RequestMapping(value = ["/saveBajasRH"], method = arrayOf(RequestMethod.POST))
    fun greeting(@RequestBody message: BajasRH): PanelBajasRH? {
        try {
            val p = PanelBajasRH( message.nombre)
            this.template.convertAndSend("/task/panelBajasRH", p)
            return p
        }
        catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }

    //Response
    @MessageMapping("/saveBajasRH")
    @SendTo("/task/panelBajasRH")
    fun greeting1(@RequestBody v: BajasRH): PanelBajasRH? {
        try {
            val p = PanelBajasRH(v.nombre)
            logger.info(p)
            return p
        } catch (e: Exception) {
            logger.error("Error al insertar")
            e.printStackTrace()
            return null
        }
    }
    companion object {

        private val logger = LogManager.getLogger(BajasRHController::class.java)
    }
}