package com.kotlinmark43.sockets.model.marketing

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Campania : Serializable {

    @NotNull
    var id: Int? = null
    var idSubRamo: Int? = null
    var nombre: String? = null
    var descripcion: String? = null
    var activo: Int? = null
    var idEstadoCampania: Int? = null
}