package com.kotlinmark43.sockets.model.marketing

data class PanelPresupuestoMarketing (

        var id: Int? = null,
        var cantidad: Int? = null,
        var mesyanio: String? = null,
        var activo: Int? = null
)