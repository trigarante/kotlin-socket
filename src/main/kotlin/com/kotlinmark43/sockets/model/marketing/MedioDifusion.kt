package com.kotlinmark43.sockets.model.marketing

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class MedioDifusion : Serializable {

    @NotNull
    var id: Long? = null
    var idSubarea: Long? = null
    var idProveedor: Int? = null
    var idExterno: Int? = null
    var idPagina: Int? = null
    var idPresupuesto: Int? = null
    var descripcion: String? = null
    var configuracion: String? = null
    var activo: Int? = null
}