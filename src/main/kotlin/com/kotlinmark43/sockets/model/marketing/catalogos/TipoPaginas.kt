package com.kotlinmark43.sockets.model.marketing.catalogos

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class TipoPaginas : Serializable {

    @NotNull
    var id: Int? = null
    var tipo:String? = null
    var objetivo: String? = null
    var activo: Int? = null
}