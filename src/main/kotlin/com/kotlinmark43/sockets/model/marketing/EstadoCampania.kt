package com.kotlinmark43.sockets.model.marketing

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class EstadoCampania : Serializable {

    @NotNull
    var id: Int? = null
    var estado: String? = null
    var activo: Int? = null
}