package com.kotlinmark43.sockets.model.marketing

data class PanelCampania (
        var id: Int? = null,
        var idSubRamo: Int? = null,
        var nombre: String? = null,
        var descripcion: String? = null,
        var activo: Int? = null,
        var idEstadoCampania: Int? = null
)