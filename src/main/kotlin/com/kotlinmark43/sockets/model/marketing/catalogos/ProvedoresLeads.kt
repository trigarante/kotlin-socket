package com.kotlinmark43.sockets.model.marketing.catalogos

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class ProvedoresLeads : Serializable {

    @NotNull
    var nombre: String? = null
}