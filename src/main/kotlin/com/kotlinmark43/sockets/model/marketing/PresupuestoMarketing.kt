package com.kotlinmark43.sockets.model.marketing

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class PresupuestoMarketing : Serializable {

    @NotNull
    var id: Int? = null
    var cantidad: Int? = null
    var mesyanio: String? = null
    var activo: Int? = null

}