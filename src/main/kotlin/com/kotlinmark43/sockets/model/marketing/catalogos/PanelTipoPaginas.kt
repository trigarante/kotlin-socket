package com.kotlinmark43.sockets.model.marketing.catalogos

data class PanelTipoPaginas (
        var id: Int? = null,
        var tipo: String? = null,
        var objetivo: String? = null,
        var activo : Int? = null
)