package com.kotlinmark43.sockets.model.marketing

data class PanelEstadoCampania (

        var id: Int? = null,
        var estado: String? = null,
        var activo: Int? = null
)