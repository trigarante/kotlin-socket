package com.kotlinmark43.sockets.model.marketing

data class PanelPaginas (

        var id: Int? = null,
        var idTipoPagina: Int? = null,
        var idCampania: Int? = null,
        var url:String? = null,
        var descripcion: String? = null,
        var fechaRegistro:String? = null,
        var fechaActualizado: String? = null,
        var activo:Int? = null,
        var idSubArea: Int? = null
)