package com.kotlinmark43.sockets.model.marketing

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class DatosNodo : Serializable {

    @NotNull
    var nombre: String? = null
}