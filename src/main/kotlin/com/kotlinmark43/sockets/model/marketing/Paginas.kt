package com.kotlinmark43.sockets.model.marketing

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Paginas : Serializable {

    @NotNull
    var id: Int? = null
    var idTipoPagina: Int? = null
    var idCampania: Int? = null
    var url:String? = null
    var descripcion: String? = null
    var fechaRegistro:String? = null
    var fechaActualizado: String? = null
    var activo:Int? = null
    var idSubArea: Int? = null
}