package com.kotlinmark43.sockets.model.marketing

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class CampanaSubArea : Serializable {

    @NotNull
    var id: Int? = null
    var idCampania: Int? = null
    var idSubarea: Int? = null
    var descripcion: String? = null
    var activo: Int? = null
    var idSubRamo:Int? = null
}