package com.kotlinmark43.sockets.model.marketing

data class PanelCampanaSubArea (

        var id: Int? = null,
        var idCampania: Int? = null,
        var idSubarea: Int? = null,
        var descripcion: String? = null,
        var activo: Int? = null,
        var idSubRamo:Int? = null
)