package com.kotlinmark43.sockets.model

data class PanelVacantes (

        var nombreVacante: String? = null,
        var grupos: String? = null,
        var empresa: String? = null,
        var sede: String? = null,
        var area: String? = null,
        var subarea: String? = null,
        var puesto: String? = null,
        var tipoPuesto: String? = null
)