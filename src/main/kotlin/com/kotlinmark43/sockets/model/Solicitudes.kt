package com.kotlinmark43.sockets.model
import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Solicitudes: Serializable {

    @NotNull
    var nombre: String? = null
    var apellidoPaterno: String? = null
    var apellidoMaterno: String? = null
    var vacante: String? = null
    var reclutador: String? = null
    var sede: String? = null
}