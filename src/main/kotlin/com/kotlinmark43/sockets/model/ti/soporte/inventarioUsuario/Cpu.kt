package com.kotlinmark43.sockets.model.ti.soporte.inventarioUsuario

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Cpu : Serializable {

    @NotNull
    var nombre: String? = null
}