package com.kotlinmark43.sockets.model.ti.administracionTI

data class PanelEjecutivo (
        var nombre: String? = null
)