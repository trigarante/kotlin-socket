package com.kotlinmark43.sockets.model.ti.administracionTI

data class PanelSupervisor (
        var nombre: String? = null
)