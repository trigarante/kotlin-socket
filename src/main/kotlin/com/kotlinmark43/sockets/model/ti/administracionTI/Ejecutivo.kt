package com.kotlinmark43.sockets.model.ti.administracionTI

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Ejecutivo: Serializable {
    @NotNull
    var nombre: String? = null
}