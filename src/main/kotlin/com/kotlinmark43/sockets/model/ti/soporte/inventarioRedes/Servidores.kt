package com.kotlinmark43.sockets.model.ti.soporte.inventarioRedes

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Servidores : Serializable {

    @NotNull
    var nombre: String? = null
}