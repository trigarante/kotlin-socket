package com.kotlinmark43.sockets.model.ti.soporte.inventarioSoporte

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Camara : Serializable {

    @NotNull
    var nombre: String? = null
}