package com.kotlinmark43.sockets.model.ti.soporte.catalogos

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class EstadoInventario : Serializable {

    @NotNull
    var nombre: String? = null
}