package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Url : Serializable {
    @NotNull
    var url: String? = null
    var descripcion: String? = null
    var activo: Number? = null

}