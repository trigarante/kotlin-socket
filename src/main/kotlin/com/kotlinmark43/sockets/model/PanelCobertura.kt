package com.kotlinmark43.sockets.model

data class PanelCobertura (
        var detalle: String? = null,
        var activo: Number? = null
)
