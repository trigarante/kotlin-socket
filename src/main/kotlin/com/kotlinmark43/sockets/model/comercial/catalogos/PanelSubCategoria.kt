package com.kotlinmark43.sockets.model.comercial.catalogos

data class PanelSubCategoria (

        var nombre: String? = null
)