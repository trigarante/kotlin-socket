package com.kotlinmark43.sockets.model.comercial.catalogos

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class BajaSocios : Serializable {

    @NotNull
    var nombre: String? = null
}