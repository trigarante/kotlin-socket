package com.kotlinmark43.sockets.model

data class PanelPrecandidatos (
        var nombre: String? = null,
        var telefono: String? = null,
        var sede: String? = null,
        var subarea: String? = null
)
