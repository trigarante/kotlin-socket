package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Precandidatos: Serializable {
    @NotNull
    var nombre: String? = null
    var telefono: String? = null
    var sede: String? = null
    var subarea: String? = null
}