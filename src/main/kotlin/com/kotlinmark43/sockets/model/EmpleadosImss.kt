package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class EmpleadosImss : Serializable {

    @NotNull
    var id: String? = null
    var idBanco: String? = null
    var idCandidato: String? = null
    var idPuesto: String? = null
    var idTipoPuesto: String? = null
    var puestoDetalle: String? = null
    var fechaIngreso: String? = null
    var sueldoDiario: String? = null
    var sueldoMensual: String? = null
    var kpi: String? = null
    var ctaClabe: String? = null
    var kpiMensual: String? = null
    var kpiTrimestral: String? = null
    var kpiSemestral: String? = null
    var comentarios: String? = null
}