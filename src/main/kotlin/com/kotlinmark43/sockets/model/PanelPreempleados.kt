package com.kotlinmark43.sockets.model

data class PanelPreempleados (
        var calificacionRollPlay: String? = null,
        var comentariosFaseDos : String? = null,
        var idEtapa: Number? = null

)