package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class RollPlay : Serializable {

    @NotNull
    var calificacionRollPlay: String? = null
    var comentarios: String? = null
    var idCapacitacion: String? = null
    var idCoach: String? = null
    var fechaIngreso: String? = null
    var idSubarea: String? = null
    var asistencia: String? = null
}