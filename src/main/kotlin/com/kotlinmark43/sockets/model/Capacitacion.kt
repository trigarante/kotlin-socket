package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Capacitacion : Serializable {

    @NotNull
    var nombre: String? = null
}