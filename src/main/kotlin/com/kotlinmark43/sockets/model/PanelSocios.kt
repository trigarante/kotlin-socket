package com.kotlinmark43.sockets.model

data class PanelSocios (

  var idEstadoSocio: Number? = null,
  var prioridad: Number? = null,
  var nombreComercial: String? = null,
  var rfc: String? = null,
  var razonSocial: String? = null,
  var alias: String? = null,
  var prioridades: String? = null,
  var estado: Number? = null,
  var activo: String? = null

)
