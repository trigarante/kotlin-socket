package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Cobertura : Serializable {
    @NotNull
    var detalle: String? = null
    var activo: Number? = null
}