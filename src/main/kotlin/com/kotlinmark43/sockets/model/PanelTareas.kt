package com.kotlinmark43.sockets.model

data class PanelTareas (

        var idEmpleados: Int? = null,
        var idSubArea: Int? = null,
        var activo: Int? = null

)