package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Candidatos : Serializable {

    @NotNull
    var comentarios: String? = null
    var calificacionPsicometrico: String? = null
    var calificacionExamen: String? = null
    var idPrecandidato: String? = null
    var fechaIngreso: String? = null
}