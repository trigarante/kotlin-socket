package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class SubRamo: Serializable {
    @NotNull
    var descripcion: String? = null;
    var prioridad: Number? = null;
    var activo: Number? = null;
}