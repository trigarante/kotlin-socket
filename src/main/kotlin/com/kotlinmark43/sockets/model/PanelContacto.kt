package com.kotlinmark43.sockets.model

data class PanelContacto (
        var nombre: String? = null,
        var activo: String? = null
)
