package com.kotlinmark43.sockets.model.VnPeru

import java.sql.Timestamp


data class PanelSolicitudesPeru (

        var id: Long = 0,
        var idCotizacionAli: Long = 0,
        var idEmpleado: Long = 0,
        var idEstadoSolicitud: Int = 0,
        var fechaSolicitud: Timestamp? = null,
        var estado: String? = null,
        var idSubArea: Int = 0,
        var subArea: String = "",
        var idTipoContacto: Int = 0,
        var nombre: String? = null,
        var apellidoPaterno: String? = null,
        var apellidoMaterno: String? = null,
        var idProducto: Int = 0,
        var idProspecto: Int = 0,
        var nombreProspecto: String? = null,
        var numeroProspecto: String? = null,
        var etiquetaSolicitud: String? = null,
        var idTipoPeticion: Long? = 0
)
