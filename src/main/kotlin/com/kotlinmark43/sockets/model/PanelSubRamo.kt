package com.kotlinmark43.sockets.model

data class PanelSubRamo (
        var descripcion: String? = null,
        var prioridad: Number? = null,
        var activo: Number? = null
)