package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Tareas : Serializable {

    @NotNull
    var idEmpleados: Int? = null
    var idSubArea: Int? = null
    var activo: Int? = null
}