package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Presupuesto : Serializable {
    @NotNull
    var presupuesto: String? = null
    var activo: String? = null
    var anio: String? = null
}