package com.kotlinmark43.sockets.model

data class PanelProducto (

        var nombre: String? = null,
        var activo: String? = null
)