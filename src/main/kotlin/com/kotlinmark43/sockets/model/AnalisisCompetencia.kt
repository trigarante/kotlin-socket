package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class AnalisisCompetencia : Serializable {
    @NotNull
    var precios: Number? = null
    var participacion: Number? = null
    var activo: Number? = null
}