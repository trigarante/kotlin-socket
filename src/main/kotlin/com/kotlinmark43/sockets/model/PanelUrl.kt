package com.kotlinmark43.sockets.model

data class PanelUrl (
        var url: String? = null,
        var descripcion: String? = null,
        var activo: Number? = null
)
