package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Vacantes : Serializable {

    @NotNull
    var nombreVacante: String? = null
    var grupos: String? = null
    var empresa: String? = null
    var sede: String? = null
    var area: String? = null
    var subarea: String? = null
    var puesto: String? = null
    var tipoPuesto: String? = null
}