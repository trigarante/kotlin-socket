package com.kotlinmark43.sockets.model

data class PanelCompetencia (
    var competencia: String? = null,
    var activo: Number? = null
)