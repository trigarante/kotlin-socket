package com.kotlinmark43.sockets.model

data class PanelAnalisisCompetencia (
        var precios: Number? = null,
        var participacion: Number? = null,
        var activo: Number? = null
)