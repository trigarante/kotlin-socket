package com.kotlinmark43.sockets.model.ventaNueva

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class VnRecibos : Serializable {

    @NotNull
    var nombre: String? = null
}