package com.kotlinmark43.sockets.model.ventaNueva.catalogos

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class VnEstadoPoliza : Serializable {

    @NotNull
    var nombre: String? = null
}