package com.kotlinmark43.sockets.model.ventaNueva

import java.sql.Date

data class  PanelAdministradorPolizas (

        var id: Long = 0,
        var idFlujoPoliza: Int = 0,
        var idEstadoRecibo: Int = 0,
        var poliza: String? = null,
        var nombreCliente: String? = null,
        var apellidoPaternoCliente: String? = null,
        var apellidoMaternoCliente: String? = null,
        var nombre: String? = null,
        var apellidoPaterno: String? = null,
        var apellidoMaterno: String? = null,
        var nombreComercial: String? = null,
        var subarea: String = "",
        var estado: String? = null,
        var estadoRecibo: String? = null,
        var fechaInicio: Date? = null,
        var numeroSerie: String? = null,
        var idRecibo: Long = 0,
        var idCliente: Long = 0,
        var archivoSubido: Int = 0,
        var idPago: Long? = 0,
        var idEstadoPago: Long? = 0,
        var idTipoPeticion: Long? = 0

)