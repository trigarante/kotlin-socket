package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Competencia : Serializable {
    @NotNull
    var competencia: String? = null
    var activo: Number? = null
}