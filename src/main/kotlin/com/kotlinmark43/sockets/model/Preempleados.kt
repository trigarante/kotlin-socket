package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import javax.validation.constraints.NotNull

@Data
class Preempleados : Serializable {

    @NotNull
    var calificacionRollPlay: String? = null
    var comentariosFaseDos : String? = null
    var idEtapa: Number? = null
}