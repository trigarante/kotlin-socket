package com.kotlinmark43.sockets.model

import lombok.Data
import java.io.Serializable
import java.sql.Timestamp
import javax.validation.constraints.NotNull

@Data
class Contrato : Serializable {
    @NotNull
    var fechaInicio: Timestamp? = null
    var fechaFin: Timestamp? = null
    var activo: Number? = null
}