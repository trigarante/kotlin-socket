package com.kotlinmark43.sockets.model.finanzas

import lombok.Data
import java.io.Serializable

@Data
class PorAplicar: Serializable {
    var id: Int? = null
    var idEstadoRecibo: Int? = null
    var poliza: String? = null
//    var idSocio: Int? = null
    var nombreComercial: String? = null
    var tipoSubRamo: String? = null
//    var idRamo: Int? = null
//    var idSubRamo: Int? = null
//    var idProductoSocio: Int? = null
}