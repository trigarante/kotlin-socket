package com.kotlinmark43.sockets.model

data class Producto (

        var nombre: String? = null,
        var activo: String? = null
)