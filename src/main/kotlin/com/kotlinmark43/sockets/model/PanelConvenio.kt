package com.kotlinmark43.sockets.model

data class PanelConvenio (
        var cantidad: Number? = null,
        var activo: Number? = null
)