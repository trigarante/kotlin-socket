package com.kotlinmark43.sockets.model

import java.sql.Timestamp

data class PanelContrato (
        var fechaInicio: Timestamp? = null,
        var fechaFin: Timestamp? = null,
        var activo: Number? = null
)
