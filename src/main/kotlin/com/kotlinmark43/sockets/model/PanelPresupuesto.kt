package com.kotlinmark43.sockets.model

data class PanelPresupuesto (
        var presupuesto: String? = null,
        var activo: String?,
        var anio: String? = null
)
