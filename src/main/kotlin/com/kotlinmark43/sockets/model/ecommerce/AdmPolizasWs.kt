package com.kotlinmark43.sockets.model.ecommerce

import lombok.Data
import java.io.Serializable

@Data
class AdmPolizasWs: Serializable {
    var id: Int? = null
    var idEstadoRecibo: Int? = null
    var estado: String? = null
    var archivoSubido: Int? = null
}