package com.kotlinmark43.sockets.model.ecommerce

data class PanelAdmPolizasWs (
        var id: Int? = null,
        var idEstadoRecibo: Int? = null,
        var estado: String? = null,
        var archivoSubido: Int? = null
)