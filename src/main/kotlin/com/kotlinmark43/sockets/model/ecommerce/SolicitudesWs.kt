package com.kotlinmark43.sockets.model.ecommerce

import lombok.Data
import java.io.Serializable

@Data
class SolicitudesWs: Serializable {
    var id: Int? = null
    var nombre: String? = null
    var apellidoPaterno: String? = null
    var apellidoMaterno: String? = null
    var estado: String? = null
}