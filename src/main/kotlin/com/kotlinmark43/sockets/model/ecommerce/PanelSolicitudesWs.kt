package com.kotlinmark43.sockets.model.ecommerce

data class PanelSolicitudesWs (
        var id: Int? = null,
        var nombre: String? = null,
        var apellidoPaterno: String? = null,
        var apellidoMaterno: String? = null,
        var estado: String? = null
)