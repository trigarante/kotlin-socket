package com.kotlinmark43.sockets.model.ecommerce.registro

//class RegistroAdminWs {
//}
import lombok.Data
import java.io.Serializable
import java.sql.Date
import javax.persistence.Basic
import javax.persistence.Column

@Data
class RegistroAdminWs: Serializable {
    var id: Long = 0
    var idFlujoPoliza: Int = 0
    var idEstadoRecibo: Int = 0
    var poliza: String? = null
    var nombreCliente: String? = null
    var apellidoPaternoCliente: String? = null
    var apellidoMaternoCliente: String? = null
    var nombre: String? = null
    var apellidoPaterno: String? = null
    var apellidoMaterno: String? = null
    var nombreComercial: String? = null
    var subarea: String = ""
    var estado: String? = null
    var estadoRecibo: String? = null
    var fechaInicio: Date? = null
    var numeroSerie: String? = null
    var idRecibo: Long = 0
    var idCliente: Long = 0
    var archivoSubido: Int = 0
    var idTipoPeticion: Long = 0
}