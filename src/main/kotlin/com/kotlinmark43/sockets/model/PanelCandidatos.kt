package com.kotlinmark43.sockets.model

data class PanelCandidatos (

        var calificacionPsicometrico: String? = null,
        var calificacionExamen: String? = null,
        var comentarios: String? = null,
        var idPrecandidato: String? = null,
        var fechaIngreso: String? = null
)