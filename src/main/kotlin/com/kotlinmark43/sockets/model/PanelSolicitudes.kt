package com.kotlinmark43.sockets.model

data class PanelSolicitudes (
        var nombre: String? = null,
        var apellidoPaterno: String? = null,
        var apellidoMaterno: String? = null,
        var vacante: String? = null,
        var reclutador: String? = null,
        var sede: String? = null
)
