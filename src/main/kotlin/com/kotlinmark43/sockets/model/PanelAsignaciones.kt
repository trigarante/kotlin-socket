package com.kotlinmark43.sockets.model

data class PanelAsignaciones (

        var calificacionRollPlay: String? = null,
        var comentarios: String? = null,
        var idCapacitacion: String? = null,
        var idCoach: String? = null,
        var fechaIngreso: String? = null,
        var idSubarea: String? = null,
        var asistencia: String? = null
)