package com.kotlinmark43.sockets

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SocketsApplication

fun main(args: Array<String>) {
	runApplication<SocketsApplication>(*args)
}
